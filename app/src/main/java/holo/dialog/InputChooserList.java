package holo.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.holo.callblocker.R;

import holo.adapter.BlacklistAdapter;
import holo.adapter.WhitelistAdapter;
import holo.components.ContactSearchActivity;
import holo.components.MainActivity;


/**
 * Created by water on 2/5/16.
 */
public class InputChooserList extends Dialog {

    MainActivity context;
    int cat;
    BlacklistAdapter blacklistAdapter;
    WhitelistAdapter whitelistAdapter;
    public InputChooserList(MainActivity context, int cat,  BlacklistAdapter blacklistAdapter, WhitelistAdapter whitelistAdapter) {
        super(context, R.style.PauseDialog);
        this.context = context;
        this.cat = cat;
        this.blacklistAdapter = blacklistAdapter;
        this.whitelistAdapter = whitelistAdapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_chooser_list);

        findViewById(R.id.fromCallsLog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputChooserList.this.dismiss();
                goToContactSearchActivity("from_calls_log");

            }
        });
        findViewById(R.id.fromContacts).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputChooserList.this.dismiss();
                goToContactSearchActivity("from_contact");
            }
        });
        findViewById(R.id.fromInputNumber).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputChooserList.this.dismiss();
               // goToContactSearchActivity("input_number");
                new InputNumber(context, cat, blacklistAdapter, whitelistAdapter).show();
            }
        });
    }

    private void goToContactSearchActivity(String option){
        Intent i = new Intent(context, ContactSearchActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("search_option", option);
        bundle.putInt("cat", cat);


        i.putExtras(bundle);
        context.startActivity(i);
    }
}
