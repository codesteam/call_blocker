package holo.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.holo.callblocker.R;

import holo.model.CatChangerListner;

/**
 * Created by water on 4/21/16.
 */
public class CategoryChanger  extends Dialog {
    Activity context;
    int cat;
    CatChangerListner catChangerListner;
    String name;
    public CategoryChanger(Activity context, CatChangerListner catChangerListner, int cat, String name) {
        super(context, R.style.PauseDialog);
        this.catChangerListner = catChangerListner;
        this.context = context;
        this.cat = cat;
        this.name = name;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_changer);

        this.setCancelable(false);
        findViewById(R.id.btnYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catChangerListner.categoryChanged();
                CategoryChanger.this.dismiss();

            }
        });

        findViewById(R.id.btnNo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catChangerListner.categoryNotChanged();
                CategoryChanger.this.dismiss();
            }
        });

        String header = "";
        String message = "";

        if (cat == 0){
            header = context.getResources().getString(R.string.move_to_black_list);
            message = context.getResources().getString(R.string.listed_in_black_list);
        }else{
            header = context.getResources().getString(R.string.move_to_white_list);
            message = context.getResources().getString(R.string.listed_in_white_list);
        }
        ((TextView) findViewById(R.id.tvHeader)).setText(header);
        ((TextView) findViewById(R.id.tvMessage)).setText(message);
        ((TextView) findViewById(R.id.tvItem)).setText(name);
    }

}