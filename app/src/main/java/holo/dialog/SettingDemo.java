package holo.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;

import com.holo.callblocker.R;

import holo.components.MainActivity;


/**
 * Created by water on 2/5/16.
 */
public class SettingDemo extends Dialog {

    MainActivity context;
    int customValue;
    int calculatedProgress;

    public SettingDemo(MainActivity context) {
        super(context,  R.style.PauseDialog);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_demo);

        findViewById(R.id.ivClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingDemo.this.dismiss();
            }
        });


    }

}