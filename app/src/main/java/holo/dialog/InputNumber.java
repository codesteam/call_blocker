package holo.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.holo.callblocker.R;

import java.util.ArrayList;

import holo.adapter.BlacklistAdapter;
import holo.adapter.WhitelistAdapter;
import holo.db.DBHelper;
import holo.model.CatChangerListner;
import holo.model.OrmContact;
import holo.util.ToastMsg;


/**
 * Created by water on 2/5/16.
 */
public class InputNumber extends Dialog {

    Activity context;
    DBHelper dbHelper;
    BlacklistAdapter blacklistAdapter;
    WhitelistAdapter whitelistAdapter;
    int cat;
    public InputNumber(Activity context, int cat, BlacklistAdapter blacklistAdapter, WhitelistAdapter whitelistAdapter) {
        super(context, R.style.PauseDialog);
        this.context = context;
        this.cat = cat;
        this.dbHelper = new DBHelper(context);
        this.blacklistAdapter = blacklistAdapter;
        this.whitelistAdapter = whitelistAdapter;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_input_number);

        final EditText etInuptNumber = (EditText) findViewById(R.id.inputNum);

        findViewById(R.id.btnAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String input = etInuptNumber.getText().toString() + "";
                if (input == null || input.length() <= 0){
                    ToastMsg.Toast(context, context.getResources().getString(R.string.please_write_your_number));
                }else{
                    final ArrayList<OrmContact> ormContacts = dbHelper.getHoloContactListByName(input);
                    if (ormContacts.size() > 0){ // since already this contact added just update the category
                        new CategoryChanger(context, new CatChangerListner() {
                            @Override
                            public void categoryChanged() {
                                for (int j = 0; j < ormContacts.size(); j++) {
                                    OrmContact hc = ormContacts.get(j);
                                    dbHelper.updateHoloContact((hc.getDatabaseId()), hc.getName(), hc.getNumbers().get(0), hc.getRefId(),
                                            System.currentTimeMillis(), cat);
                                    if (cat == 1){
                                        ToastMsg.Toast(context,
                                                context.getResources().getString(R.string.move_to_white_list_successfully));

                                    }else{
                                        ToastMsg.Toast(context,
                                                context.getResources().getString(R.string.move_to_black_list_successfully));

                                    }
                                    if (cat == 0){
                                        blacklistAdapter.resetAdapter();
                                    }else{
                                        whitelistAdapter.resetAdapter();
                                    }

                                    etInuptNumber.setText("");
                                }
                            }

                            @Override
                            public void categoryNotChanged() {

                            }
                        }, cat, input).show();
                    }else{
                        dbHelper.insertHoloContact(input, input, System.currentTimeMillis() + "", System.currentTimeMillis(), cat);
                        if (cat == 0){
                            blacklistAdapter.resetAdapter();
                        }else{
                            whitelistAdapter.resetAdapter();
                        }
                        InputNumber.this.dismiss();
                    }


                }
                InputNumber.this.dismiss();
            }
        });

        findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputNumber.this.dismiss();
            }
        });
    }

}
