package holo.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.holo.callblocker.R;

import java.util.Date;

import holo.adapter.BlacklistAdapter;
import holo.components.MainActivity;
import holo.datetime.DateTimeConverter;
import holo.db.DBHelper;
import holo.model.OrmContact;
import holo.util.Print;
import holo.util.ToastMsg;


/**
 * Created by water on 2/5/16.
 */
public class SettingBlacklistItem extends Dialog {

    MainActivity context;
    OrmContact ormContact;
    android.support.v7.widget.SwitchCompat swBlockUntil;
    DBHelper mydb;
    android.support.v7.widget.SwitchCompat swSendSMS;
    //DateTimePicker dateTimePicker;
    BlacklistAdapter blacklistAdapter;
    EditText etWriteSMS;
    Button btnSave;
    Button btnClear;
    TextView tvBlockTime;


    private RadioGroup radioGroup;
    private RadioButton rbWarnningMessage, rbRemoveFromList;

    private SlideDateTimeListener listener = new SlideDateTimeListener() {

        @Override
        public void onDateTimeSet(Date date)
        {
            long pickedTime = DateTimeConverter.convertDateToMillis(date);
            mydb.updateHoloContact(ormContact.getDatabaseId(), DBHelper.CONTACTS_COLUMN_BLOCK_UNTIL_TIME, pickedTime+"");
            ormContact.setBlockUntilTime(pickedTime);
            blacklistAdapter.resetAdapter();
            setTvBlockTime();
            Print.e(this, "pickedTime: " +pickedTime);
        }

        // Optional cancel listener
        @Override
        public void onDateTimeCancel()
        {
            mydb.updateHoloContact(ormContact.getDatabaseId(), DBHelper.CONTACTS_COLUMN_BLOCK_UNTIL_TIME, 0 + "");
            blacklistAdapter.resetAdapter();
            ormContact.setBlockUntilTime(0);
            setTvBlockTime();
            //Toast.makeText(SampleActivity.this,
               //     "Canceled", Toast.LENGTH_SHORT).show();
        }
    };
    public SettingBlacklistItem(MainActivity context, OrmContact ormContact, BlacklistAdapter blacklistAdapter) {
        super(context,  R.style.PauseDialog);
        this.context = context;
        this.ormContact = ormContact;
        mydb = new DBHelper(this.context);
        this.blacklistAdapter = blacklistAdapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_blacklist_item);

        findViewById(R.id.ivClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingBlacklistItem.this.dismiss();
            }
        });
        ((TextView) findViewById(R.id.tvHeader)).setText(ormContact.getName());
        radioGroup = (RadioGroup) findViewById(R.id.radioAfterTimeOut);
        rbRemoveFromList = (RadioButton) findViewById(R.id.radioRemoveItem);
        rbWarnningMessage = (RadioButton) findViewById(R.id.radioShowWarnning);
        tvBlockTime = (TextView) findViewById(R.id.tvBlockTime);

        etWriteSMS = (EditText) findViewById(R.id.etWriteMessage);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnClear = (Button) findViewById(R.id.btnClear);

        swSendSMS = (android.support.v7.widget.SwitchCompat) findViewById(R.id.switchSendSMS);
        swBlockUntil = (android.support.v7.widget.SwitchCompat) findViewById(R.id.switchBlockUntil);
        final LinearLayout llWriteSMSViewArea = (LinearLayout) findViewById(R.id.llWriteMessageViewArea);
        final LinearLayout llBlockUntilViewArea = (LinearLayout) findViewById(R.id.llBlockUntilViewArea);

        final RelativeLayout rlWriteSMS = (RelativeLayout) findViewById(R.id.rlSendSMS);
        final RelativeLayout rlBlockUntil = (RelativeLayout) findViewById(R.id.rlBlockUntil);

        if (ormContact.getSendMessage() == 1){
            swSendSMS.setChecked(true);
            uiSendSms(llWriteSMSViewArea, View.VISIBLE, 1);
            if (ormContact.getMessage() != null){
                etWriteSMS.setText(ormContact.getMessage());
            }
        }else{
            swSendSMS.setChecked(false);
            uiSendSms(llWriteSMSViewArea, View.GONE, 0);
        }

        if (ormContact.getBlockUntile() == 1){
            swBlockUntil.setChecked(true);
            uiBlockUntil(llBlockUntilViewArea, View.VISIBLE, 1);
        }else{
            swBlockUntil.setChecked(false);
            uiBlockUntil(llBlockUntilViewArea, View.GONE, 0);
        }

        rlBlockUntil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = !swBlockUntil.isChecked();
                swBlockUntil.setChecked(isChecked);
                if (isChecked) {
                    uiBlockUntil(llBlockUntilViewArea, View.VISIBLE, 1);
                } else {
                    uiBlockUntil(llBlockUntilViewArea, View.GONE, 0);
                    mydb.updateHoloContact(ormContact.getDatabaseId(), DBHelper.CONTACTS_COLUMN_BLOCK_UNTIL_TIME, 0 + "");
                }
            }
        });

        rlWriteSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = !swSendSMS.isChecked();
                Print.e(this, isChecked);
                swSendSMS.setChecked(isChecked);
                if (isChecked) {
                    uiSendSms(llWriteSMSViewArea, View.VISIBLE, 1);
                } else {
                    uiSendSms(llWriteSMSViewArea, View.GONE, 0);
                    mydb.updateHoloContact(ormContact.getDatabaseId(), DBHelper.CONTACTS_COLUMN_SEND_SMS, null);
                    etWriteSMS.setText("");
                }
            }
        });
      /*  swSendSMS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    uiSendSms(llWriteSMSViewArea, View.VISIBLE, 1);
                } else {
                    uiSendSms(llWriteSMSViewArea, View.GONE, 0);
                    mydb.updateHoloContact(ormContact.getDatabaseId(), DBHelper.CONTACTS_COLUMN_SEND_SMS, null);
                    etWriteSMS.setText("");
                }

            }
        });

        swBlockUntil.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    uiBlockUntil(llBlockUntilViewArea, View.VISIBLE, 1);
                } else {
                    uiBlockUntil(llBlockUntilViewArea, View.GONE, 0);
                    mydb.updateHoloContact(ormContact.getDatabaseId(), DBHelper.CONTACTS_COLUMN_BLOCK_UNTIL_TIME, 0 + "");
                }

            }
        });*/

        findViewById(R.id.llSetTime).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SlideDateTimePicker.Builder(context.getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                                //.setMinDate(minDate)
                                //.setMaxDate(maxDate)
                                //.setIs24HourTime(true)
                        .setTheme(SlideDateTimePicker.HOLO_LIGHT)
                                //.setIndicatorColor(Color.parseColor("#990000"))
                        .build()
                        .show();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = etWriteSMS.getText().toString().trim();

                if (text.length() > 0) {
                    mydb.updateHoloContact(ormContact.getDatabaseId(), DBHelper.CONTACTS_COLUMN_SMS, text);
                    blacklistAdapter.resetAdapter();
                    ToastMsg.Toast(context, R.string.tst_message_saved_successfully);
                } else {
                    mydb.updateHoloContact(ormContact.getDatabaseId(), DBHelper.CONTACTS_COLUMN_SMS, null);
                    blacklistAdapter.resetAdapter();
                    ToastMsg.Toast(context, R.string.tst_please_write_message);
                }
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mydb.updateHoloContact(ormContact.getDatabaseId(), DBHelper.CONTACTS_COLUMN_SMS, null);
                blacklistAdapter.resetAdapter();
                etWriteSMS.setText("");
                ToastMsg.Toast(context, R.string.tst_cleaning_done);
            }
        });
        setTvBlockTime();


        if (ormContact.getAfterTimeOut() == 0){
            rbWarnningMessage.setChecked(true);
        }else{
            rbRemoveFromList.setChecked(true);
        }
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.radioShowWarnning) {
                    mydb.updateHoloContact(ormContact.getDatabaseId(), DBHelper.CONTACTS_COLUMN_AFTER_TIME_OUT, 0+"");
                    blacklistAdapter.resetAdapter();
                    ormContact.setAfterTimeOut(0);
                } else if (checkedId == R.id.radioRemoveItem) {
                    mydb.updateHoloContact(ormContact.getDatabaseId(), DBHelper.CONTACTS_COLUMN_AFTER_TIME_OUT, 1 + "");
                   // holoContactlistAdapter.resetAdapter();
                    ormContact.setAfterTimeOut(1);
                }
            }

        });
        Print.e(this, " getBlockUntile: " + ormContact.getBlockUntile());
        Print.e(this, " getBlockUntilTime: "+ ormContact.getBlockUntilTime());
        Print.e(this, " getSendMessage: "+ ormContact.getSendMessage());
        Print.e(this, " getMessage: "+ ormContact.getMessage());
        Print.e(this, " getAfterTimeOut: "+ ormContact.getAfterTimeOut());

    }

    Handler handler = new Handler();

    final Runnable r = new Runnable() {
        public void run() {
            blacklistAdapter.resetAdapter();
        }
    };
    private void setTvBlockTime(){

        tvBlockTime.setTextColor(context.getResources().getColor(android.R.color.secondary_text_dark));

        if(ormContact.getBlockUntilTime()> 0 && ormContact.getBlockUntile() > 0){
            if (ormContact.getBlockUntilTime() > System.currentTimeMillis()){
                tvBlockTime.setText("Block Until: "+DateTimeConverter.getFormatedDate(ormContact.getBlockUntilTime(), "dd/MM/yyyy hh:mm: aa"));
                tvBlockTime.setTextColor(context.getResources().getColor(R.color.Green));
            }else{
                tvBlockTime.setText("Time out: "+DateTimeConverter.getFormatedDate(ormContact.getBlockUntilTime(), "dd/MM/yyyy hh:mm: aa"));
                tvBlockTime.setTextColor(context.getResources().getColor(R.color.Warnning));
            }
        }else{
            tvBlockTime.setText(context.getResources().getString(R.string.please_set_date_and_time));
        }
    }
    private void uiSendSms(LinearLayout ll, int visibility, int isSendSMS){
        ll.setVisibility(visibility);
        mydb.updateHoloContact(ormContact.getDatabaseId(), DBHelper.CONTACTS_COLUMN_SEND_SMS, isSendSMS + "");
        ormContact.setSendMessage(isSendSMS);
        blacklistAdapter.resetAdapter();
    }

    private void uiBlockUntil(LinearLayout ll, int visibility, int isBlockUntile){
        ll.setVisibility(visibility);
        mydb.updateHoloContact(ormContact.getDatabaseId(), DBHelper.CONTACTS_COLUMN_BLOCK_UNTIL, isBlockUntile + "");
        ormContact.setBlockUntile(isBlockUntile);
        blacklistAdapter.resetAdapter();
    }

}