package holo.model;

/**
 * Created by water on 4/17/16.
 */
public class BlockCallLog {
    int id;
    String name;
    String number;
    long time;
    public BlockCallLog(int id, String name, String number, long time){
        this.name = name;
        this.time = time;
        this.number = number;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public long getTime() {
        return time;
    }

    public String getNumber() {
        return number;
    }

    public int getId() {
        return id;
    }
}
