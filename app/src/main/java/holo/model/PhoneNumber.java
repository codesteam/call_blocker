package holo.model;

/**
 * Created by water on 4/17/16.
 */
public class PhoneNumber extends HoloContact{
    String phone;
    public PhoneNumber(String phone, String name){
        this.phone = phone;
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }

    public int getSendMessage() {
        return sendMessage;
    }

    public void setSendMessage(int sendMessage) {
        this.sendMessage = sendMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getBlockUntile() {
        return blockUntile;
    }

    public void setBlockUntile(int blockUntile) {
        this.blockUntile = blockUntile;
    }

    public long getBlockUntilTime() {
        return blockUntilTime;
    }

    public void setBlockUntilTime(long blockUntilTime) {
        this.blockUntilTime = blockUntilTime;
    }

    public int getAfterTimeOut() {
        return afterTimeOut;
    }

    public void setAfterTimeOut(int afterTimeOut) {
        this.afterTimeOut = afterTimeOut;
    }
}
