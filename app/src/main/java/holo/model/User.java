package holo.model;

/**
 * Created by water on 4/24/16.
 */
public class User {
    String password;
    String hint;
    String email;
    int id;
    public User(int id, String password, String hint,  String email){
        this.password = password;
        this.hint = hint;
        this.id = id;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public String getHint() {
        return hint;
    }


    public String getEmail() {
        return email;
    }
}
