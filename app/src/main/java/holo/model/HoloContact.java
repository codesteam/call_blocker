package holo.model;

/**
 * Created by water on 4/21/16.
 */
public class HoloContact {
    String name;
    protected  int sendMessage; // 0 for not enabled send message, 1 for enabled send message
    protected String message ; // null-for no message set yet
    protected int blockUntile; // 0 for not enable block until, 1 for enbaled block until
    protected long blockUntilTime;
    protected int afterTimeOut; // 0 for show warrining message, 1 for Remove from blacklist
}
