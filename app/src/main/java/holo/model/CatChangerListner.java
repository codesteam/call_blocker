package holo.model;

/**
 * Created by water on 4/21/16.
 */
public interface CatChangerListner {
    void categoryChanged();
    void categoryNotChanged();
}
