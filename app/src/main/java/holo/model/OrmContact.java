package holo.model;

import java.util.ArrayList;

/**
 * Created by water on 3/17/16.
 */
public class OrmContact extends  HoloContact{
    int databaseId;
    String refId;
    boolean selected;
    ArrayList<String> numbers;

    int type; // 1 for call logs, 2 for contacts // after saveing a black list item type has no work, means we not saving type in database
    int cat;// 0 for balcklist 1 for white list // savig value into database after user select a contact as balck or white list
    public OrmContact(String name, String refId, int cat){
        this.name = name;
        this.refId = refId;
        this.cat = cat;
    }

    public int getSendMessage() {
        return sendMessage;
    }

    public void setSendMessage(int sendMessage) {
        this.sendMessage = sendMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getBlockUntile() {
        return blockUntile;
    }

    public void setBlockUntile(int blockUntile) {
        this.blockUntile = blockUntile;
    }

    public long getBlockUntilTime() {
        return blockUntilTime;
    }

    public void setBlockUntilTime(long blockUntilTime) {
        this.blockUntilTime = blockUntilTime;
    }

    public int getAfterTimeOut() {
        return afterTimeOut;
    }

    public void setAfterTimeOut(int afterTimeOut) {
        this.afterTimeOut = afterTimeOut;
    }

    public void setDatabaseId(int databaseId) {
        this.databaseId = databaseId;
    }

    public int getDatabaseId() {
        return databaseId;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getRefId() {
        return refId;
    }

    public String getName() {
        return name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setNumbers(ArrayList<String> numbers) {
        this.numbers = numbers;
    }

    public ArrayList<String> getNumbers() {
        return numbers;
    }

    public int getType() {
        return type;
    }


}
