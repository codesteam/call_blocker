package holo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import holo.model.BlockCallLog;
import holo.model.OrmContact;
import holo.model.PhoneNumber;
import holo.model.User;

/**
 * Created by water on 3/19/16.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "callblocker.db";
    public static final String CONTACTS_TABLE_NAME = "contacts";
    public static final String USER_TABLE_NAME = "user";

    public static final String CONTACTS_COLUMN_ID = "id";
    public static final String CONTACTS_COLUMN_NAME = "name";
    public static final String CONTACTS_COLUMN_REF_ID = "refID";
    public static final String CONTACTS_COLUMN_LOG_TIME = "time";
    public static final String CONTACTS_COLUMN_CAT = "cat"; // 0 for blacklist 1 for whitelist
    public static final String CONTACTS_COLUMN_PHONE = "phone";

    public static final String CONTACTS_COLUMN_SEND_SMS = "send_sms";
    public static final String CONTACTS_COLUMN_SMS = "sms";
    public static final String CONTACTS_COLUMN_BLOCK_UNTIL = "block_until";
    public static final String CONTACTS_COLUMN_BLOCK_UNTIL_TIME = "block_until_time";
    public static final String CONTACTS_COLUMN_AFTER_TIME_OUT = "after_time_out";


    public static final String USER_COLUMN_ID = "id";
    public static final String USER_COLUMN_PASSWORD = "password";
    public static final String USER_COLUMN_HINT = "hint";
    public static final String USER_COLUMN_EMAIL = "email";
    private HashMap hp;

    String queryContacts =  "create table contacts " +
            "(id integer primary key, name text,phone text NOT NULL UNIQUE,refID text, time INTEGER,cat INTEGER" +
            ",send_sms INTEGER, sms text,block_until INTEGER,block_until_time INTEGER,after_time_out INTEGER)";

    String queryLogs =  "create table blacklogs " +
            "(id integer primary key NOT NULL, name text, phone text, time INTEGER)";

    String user =  "create table user " +
            "(id integer primary key NOT NULL, password text, hint text, email text)";

    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(queryContacts);
        db.execSQL(queryLogs);
        db.execSQL(user);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS contacts");
        db.execSQL("DROP TABLE IF EXISTS blacklogs");
        onCreate(db);
    }

    public boolean insertUser(String password, String hint, String email)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(USER_COLUMN_PASSWORD, password);
        contentValues.put(USER_COLUMN_HINT, hint);
        contentValues.put(USER_COLUMN_EMAIL, email);
        try{
            db.insertOrThrow("user", null, contentValues);
        }catch (Exception e){
            e.printStackTrace();
        }

        return true;
    }


    public boolean insertHoloContact(String name, String phone, String refID, long time, int cat)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("phone", phone);
        contentValues.put("refID", refID);
        contentValues.put("time", time);
        contentValues.put("cat", cat);

        //contentValues.put("place", place);
        try{
            db.insertOrThrow("contacts", null, contentValues);
        }catch (Exception e){
            e.printStackTrace();
        }

        return true;
    }

    public boolean insertLogs  (String name, String phone, long time)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("phone", phone);
        contentValues.put("time", time);
        try{
            db.insertOrThrow("blacklogs", null, contentValues);
        }catch (Exception e){
            e.printStackTrace();
        }

        return true;
    }
    public Cursor getData(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from contacts where id="+id+"", null );
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, CONTACTS_TABLE_NAME);
        return numRows;
    }

    public boolean updateHoloContact (Integer id, String name, String phone, String refID, long time, int cat)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("phone", phone);
        contentValues.put("refID", refID);
        contentValues.put("time", time);
        contentValues.put("cat", cat);

        // contentValues.put("street", street);
        //contentValues.put("place", place);
        db.update("contacts", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public boolean updateHoloContact (Integer id, String columnName, String value)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(columnName, value);
        db.update("contacts", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }
    public Integer deleteHoloContact (Integer id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("contacts",
                "id = ? ",
                new String[] { Integer.toString(id) });
    }

    public Integer deleteBlockCallLog(Integer id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("blacklogs",
                "id = ? ",
                new String[] { Integer.toString(id) });
    }
    public Integer deleteHoloContactByName(String name)
    {/*
        String sql = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + BANGLA + " LIKE ? ORDER BY " + BANGLA + " LIMIT 100";*/
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("contacts",
                "name LIKE ? ",
                new String[]{name + "%"});
    }

    public ArrayList<OrmContact> getHoloContactList(int cat) // 0 for blacklist 1 for white list
    {
        ArrayList<OrmContact> array_list = new ArrayList<OrmContact>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        //Cursor res = db.rawQuery("select * from contacts" + " group by name" + " ORDER BY time DESC", null);

        Cursor res =  db.rawQuery( "select * from contacts where cat="+cat +" group by name"+ " ORDER BY time DESC", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            OrmContact c = new OrmContact(res.getString(res.getColumnIndex(CONTACTS_COLUMN_NAME)),
                    res.getString(res.getColumnIndex(CONTACTS_COLUMN_REF_ID))+"",
                    res.getInt(res.getColumnIndex(CONTACTS_COLUMN_CAT)));

            c.setDatabaseId(res.getInt(res.getColumnIndex(CONTACTS_COLUMN_ID)));
            c.setSendMessage(res.getInt(res.getColumnIndex(CONTACTS_COLUMN_SEND_SMS)));
            c.setMessage(res.getString(res.getColumnIndex(CONTACTS_COLUMN_SMS)));
            c.setBlockUntile(res.getInt(res.getColumnIndex(CONTACTS_COLUMN_BLOCK_UNTIL)));
            c.setBlockUntilTime(res.getLong(res.getColumnIndex(CONTACTS_COLUMN_BLOCK_UNTIL_TIME)));
            c.setAfterTimeOut(res.getInt(res.getColumnIndex(CONTACTS_COLUMN_AFTER_TIME_OUT)));
            //Print.e(this, "Cat: " +res.getInt(res.getColumnIndex(CONTACTS_COLUMN_CAT)));
            //Print.e(this, "Name: " +res.getInt(res.getColumnIndex(CONTACTS_COLUMN_NAME)));
            array_list.add(c);
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<OrmContact> getHoloContactListByName(String name) // 0 for blacklist 1 for white list
    {
        ArrayList<OrmContact> array_list = new ArrayList<OrmContact>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res =  db.rawQuery("select * from contacts where name like '%"+name + "%'", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            OrmContact c = new OrmContact(res.getString(res.getColumnIndex(CONTACTS_COLUMN_NAME)),
                    res.getString(res.getColumnIndex(CONTACTS_COLUMN_REF_ID))+"",
                    res.getInt(res.getColumnIndex(CONTACTS_COLUMN_CAT)));
            c.setSendMessage(res.getInt(res.getColumnIndex(CONTACTS_COLUMN_SEND_SMS)));
            c.setMessage(res.getString(res.getColumnIndex(CONTACTS_COLUMN_SMS)));
            c.setBlockUntile(res.getInt(res.getColumnIndex(CONTACTS_COLUMN_BLOCK_UNTIL)));
            c.setBlockUntilTime(res.getLong(res.getColumnIndex(CONTACTS_COLUMN_BLOCK_UNTIL_TIME)));
            c.setAfterTimeOut(res.getInt(res.getColumnIndex(CONTACTS_COLUMN_AFTER_TIME_OUT)));
            ArrayList<String> nums = new ArrayList<>();
            nums.add(res.getString(res.getColumnIndex(CONTACTS_COLUMN_PHONE)));
            c.setNumbers(nums);
            c.setDatabaseId(res.getInt(res.getColumnIndex(CONTACTS_COLUMN_ID)));

            array_list.add(c);
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<PhoneNumber> getBlackNumbers()
    {
        ArrayList<PhoneNumber> array_list = new ArrayList<PhoneNumber>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from contacts where cat=0" +" group by name"+ " ORDER BY time DESC", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            PhoneNumber c = new PhoneNumber(res.getString(res.getColumnIndex(CONTACTS_COLUMN_PHONE)),
                    res.getString(res.getColumnIndex(CONTACTS_COLUMN_NAME)));

            c.setSendMessage(res.getInt(res.getColumnIndex(CONTACTS_COLUMN_SEND_SMS)));
            c.setMessage(res.getString(res.getColumnIndex(CONTACTS_COLUMN_SMS)));
            c.setBlockUntile(res.getInt(res.getColumnIndex(CONTACTS_COLUMN_BLOCK_UNTIL)));
            c.setBlockUntilTime(res.getLong(res.getColumnIndex(CONTACTS_COLUMN_BLOCK_UNTIL_TIME)));
            c.setAfterTimeOut(res.getInt(res.getColumnIndex(CONTACTS_COLUMN_AFTER_TIME_OUT)));

            array_list.add(c);
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<BlockCallLog> getBlockLogs()
    {
        ArrayList<BlockCallLog> array_list = new ArrayList<BlockCallLog>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from blacklogs" + " ORDER BY time DESC", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            BlockCallLog c = new BlockCallLog(
                    res.getInt(res.getColumnIndex(CONTACTS_COLUMN_ID)),
                    res.getString(res.getColumnIndex(CONTACTS_COLUMN_NAME)),
                    res.getString(res.getColumnIndex(CONTACTS_COLUMN_PHONE)),
                    res.getLong(res.getColumnIndex(CONTACTS_COLUMN_LOG_TIME)));
            array_list.add(c);
            res.moveToNext();
        }
        Log.e("adfaf", "Size: "+ array_list.size());
        return array_list;
    }

    public void deleteBlockLogs ()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + "blacklogs");
    }

    public ArrayList<User> getUser()
    {
        ArrayList<User> array_list = new ArrayList<User>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from user", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            User c = new User(
                    res.getInt(res.getColumnIndex(USER_COLUMN_ID)),
                    res.getString(res.getColumnIndex(USER_COLUMN_PASSWORD)),
                    res.getString(res.getColumnIndex(USER_COLUMN_HINT)),
                    res.getString(res.getColumnIndex(USER_COLUMN_EMAIL)));
            array_list.add(c);
            res.moveToNext();
        }
        return array_list;
    }

    public void deleteUser ()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + "user");
    }
}