package holo.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import holo.common.CommonSettings;

/**
 * Created by water on 4/20/16.
 */
public class SchedulerFiltter {

    public boolean isShedulerOn(){
        return CommonSettings.getInstance().getKEY_SCHEDULER_ENABLE();
    }
    public boolean cheekSchedule(){ // time is block by scheduler then return false, else return true
        boolean isTodayScheduled = isTodayScheduled(); // if false then no need to call block today, pass the call block

        if (!isTodayScheduled){
            Print.e(this, "today is not scheduled ");
            return false;
        }else{ // need to cheek current time is in time bounds
            Print.e(this, "today is  scheduled ");
            if(!isTodayTimeScheduled()){
                Print.e(this, "this time is not scheduled ");
                return false; // no need to call block
            }else{
                Print.e(this, "this time is scheduled ");
                return true; // check for call block
            }
        }
    }

    private boolean isTodayTimeScheduled(){
        try {
            String strFrom = get24HrFormate(CommonSettings.getInstance().getKEY_SCHEDULER_FROM());
            Print.e(this, "strFrom: "+strFrom);
            Date timeFrom = new SimpleDateFormat("HH:mm").parse(strFrom);
            Calendar calendarFrom = Calendar.getInstance();
            calendarFrom.setTime(timeFrom);
            Print.e(this, "From time: " + calendarFrom.get(Calendar.HOUR_OF_DAY) + ":" + calendarFrom.get(Calendar.MINUTE));
            String strTo = get24HrFormate(CommonSettings.getInstance().getKEY_SCHEDULER_TO());
           Print.e(this, "strTo: " + strTo);

            Date timeTo = new SimpleDateFormat("HH:mm").parse(strTo);
            Calendar calendarTo = Calendar.getInstance();
            calendarTo.setTime(timeTo);
            calendarTo.add(Calendar.DATE, 1);
            Print.e(this, "To time: " + calendarTo.get(Calendar.HOUR_OF_DAY) + ":" + calendarTo.get(Calendar.MINUTE));

            String currentTime = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)+":"+
                    Calendar.getInstance().get(Calendar.MINUTE);

            Print.e(this, "Current time: " + currentTime);

            Date d = new SimpleDateFormat("HH:mm").parse(currentTime);

            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(d);
            calendar3.add(Calendar.DATE, 1);

            Date x = calendar3.getTime();
            if (strFrom.equals(strTo)) {
                return  true;
            }
            else if (x.after(calendarFrom.getTime()) && x.before(calendarTo.getTime())) {
                //checkes whether the current time is between 14:49:00 and 20:11:13.
               // System.out.println(true);
                return  true;
            }
        } catch (Exception e) {
            Print.e(this, "Exception: ");

            e.printStackTrace();
        }

        return  false;
    }

    private String get24HrFormate(String formate12){
        DateFormat readFormat = new SimpleDateFormat( "hh:mm aa");
        DateFormat writeFormat = new SimpleDateFormat( "HH:mm");
        Date date = null;
        try
        {
            date = readFormat.parse( formate12 );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
        String formattedDate = null;
        if( date != null )
        {
             formattedDate = writeFormat.format( date );
        }
        return  formattedDate;
    }
    private boolean isTodayScheduled(){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        boolean isTodayScheduled = false;
        switch (day) {
            case Calendar.MONDAY:
                isTodayScheduled = CommonSettings.getInstance().getKEY_SCHEDULER_MONDAY();
                break;
            case Calendar.TUESDAY:
                isTodayScheduled = CommonSettings.getInstance().getKEY_SCHEDULER_TUESDAY();
                break;
            case Calendar.WEDNESDAY:
                isTodayScheduled = CommonSettings.getInstance().getKEY_SCHEDULER_WEDNESDAY();
                break;
            case Calendar.THURSDAY:
                isTodayScheduled = CommonSettings.getInstance().getKEY_SCHEDULER_THURSDAY();
                break;
            case Calendar.FRIDAY:
                isTodayScheduled = CommonSettings.getInstance().getKEY_SCHEDULER_FRIDAY();
                break;
            case Calendar.SATURDAY:
                isTodayScheduled = CommonSettings.getInstance().getKEY_SCHEDULER_SATURDAY();
                break;
            case Calendar.SUNDAY:
                isTodayScheduled = CommonSettings.getInstance().getKEY_SCHEDULER_SUNDAY();
        }

        return  isTodayScheduled;
    }
}
