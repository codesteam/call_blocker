package holo.sm;

import android.support.v7.app.ActionBarActivity;

import com.holo.callblocker.R;

import holo.common.CommonConstants;
import holo.fragments.Scheduler;
import holo.util.ConnectionDetector;
import holo.util.ToastMsg;
import materialdesign.model.Divider;
import materialdesign.model.NavDrawerItem;


public class SmScheduler extends NavDrawerItem {

	public SmScheduler()
	{

	}
	int container;

	ActionBarActivity context;
    public SmScheduler(ActionBarActivity context, boolean showNotify, String title, String subTitle, int selectorImageView,
					   int textSize, int textColor, int container, Divider... params) {
        super(context, showNotify, title, subTitle, selectorImageView, textSize, textColor, params );

		this.context = context;
		this.container = container;
    }


	@Override
	public void fire()
	{
		if (CommonConstants.REQUIRED_INTERNET_CONNECTION){
			if (ConnectionDetector.isNetworkPresent(context)){
				action();
			}else{
                ToastMsg.Toast(context, R.string.tst_connection_lost);
            }
		}else{
			action();
		}


	}

	private void action(){
		super.fire();
		android.support.v4.app.FragmentManager fm = context.getSupportFragmentManager();
		int count = fm.getBackStackEntryCount();
		for (int i = 0; i < count; i++){
			fm.popBackStack();
		}
		android.support.v4.app.FragmentTransaction transaction = fm.beginTransaction();
		transaction.replace(container, new Scheduler());
		transaction.commit();
	}

}
