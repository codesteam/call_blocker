package holo.common;

public class CommonConstants {
	public static final int AD_WAIT_IN_SEC = 45;
    public static final boolean REQUIRED_INTERNET_CONNECTION = false;
    public static final boolean SHOW_PROFILER_IMAGE = false;
    public static final int MIN_REQUEST_COUNTER_LIMITE = 5;
}
