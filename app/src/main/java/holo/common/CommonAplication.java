package holo.common;

import android.app.Application;
import android.content.Context;

/**
 * Created by water on 5/14/15.
 */


public class CommonAplication extends Application {

    private static Context mContext;

    public static Context getAppContext() {
        return CommonAplication.mContext;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        CommonAplication.mContext = getApplicationContext();

    }


}
