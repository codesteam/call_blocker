package holo.common;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class CommonSettings {


    public static String PACKAGE_NAME;
    private static CommonSettings singleton = null;
    public String KEY_SD_CARD_STATE= ".KEY_SD_CARD_STATE";
    public String KEY_USER_RINGER_MODE= ".KEY_SD_CARD_STATE";

    public String KEY_SCHEDULER_ENABLE= ".KEY_SCHEDULER_ENABLE";
    public String KEY_SCHEDULER_FROM= ".KEY_SCHEDULER_FROM";
    public String KEY_SCHEDULER_TO= ".KEY_SCHEDULER_TO";

    public String KEY_SCHEDULER_SUNDAY= ".KEY_SCHEDULER_SUNDAY";
    public String KEY_SCHEDULER_MONDAY= ".KEY_SCHEDULER_MONDAY";
    public String KEY_SCHEDULER_TUESDAY= ".KEY_SCHEDULER_TUESDAY";
    public String KEY_SCHEDULER_WEDNESDAY= ".KEY_SCHEDULER_WEDNESDAY";
    public String KEY_SCHEDULER_THURSDAY= ".KEY_SCHEDULER_THURSDAY";
    public String KEY_SCHEDULER_FRIDAY= ".KEY_SCHEDULER_FRIDAY";
    public String KEY_SCHEDULER_SATURDAY= ".KEY_SCHEDULER_SATURDAY";


    public String KEY_MESSAGE_SENT_ENABLE= ".KEY_MESSAGE_SENT_ENABLE";
    public String KEY_MESSAGE= ".KEY_MESSAGE";
    public String KEY_CALL_BLOCKER_ENABLE= ".KEY_CALL_BLOCKER_ENABLE";


    public String KEY_BLOCK_PRIVATE_NUMBERS= ".KEY_BLOCK_PRIVATE_NUMBERS";
    public String KEY_BLOCK_UNKNOWN_NUMBERS= ".KEY_BLOCK_UNKNOWN_NUMBERS";
    public String KEY_BLOCK_ALL_CALLS= ".KEY_BLOCK_ALL_CALLS";


    public String KEY_SHOW_NOTIFICATION= ".KEY_SHOW_NOTIFICATION";
    public String KEY_STATUS_BAR_ICON= ".KEY_STATUS_BAR_ICON";

    private SharedPreferences _prefs;
    private SharedPreferences.Editor _editor;
    //private static Context context;
    private CommonSettings() {
        PACKAGE_NAME = CommonAplication.getAppContext().getPackageName();
        _prefs = PreferenceManager.getDefaultSharedPreferences(CommonAplication.getAppContext());
        _editor = _prefs.edit();
    }
    public static void createInstance(Activity context){
       // CommonSettings.context = context;
        singleton = new CommonSettings();
    }

    public static CommonSettings getInstance() {
        if (singleton == null){
           // context = CommonAplication.getAppContext();
            singleton = new CommonSettings();
        }
        return singleton;
    }


    @Override
    protected Object clone() throws CloneNotSupportedException {

        return new CloneNotSupportedException();
    }

    public void setString(String key, String value){
        _editor.putString(key, value);
        _editor.commit();
    }

    public String getString(String key){
        return _prefs.getString(key, null);
    }


    public void setInt(String key, int value){
        _editor.putInt(key, value);
        _editor.commit();
    }

    public int getInt(String key){
        return _prefs.getInt(key, 0);
    }

    public void setKEY_SCHEDULER_ENABLE(boolean KEY_SCHEDULER_ENABLE) {
        _editor.putBoolean(PACKAGE_NAME + this.KEY_SCHEDULER_ENABLE, KEY_SCHEDULER_ENABLE);
        _editor.commit();
    }

    public boolean getKEY_SCHEDULER_ENABLE() {
        return  _prefs.getBoolean(PACKAGE_NAME + KEY_SCHEDULER_ENABLE, false);
    }

    public void setKEY_SCHEDULER_TO(String KEY_SCHEDULER_TO) {
        _editor.putString(PACKAGE_NAME + this.KEY_SCHEDULER_TO, KEY_SCHEDULER_TO);
        _editor.commit();
    }

    public String getKEY_SCHEDULER_TO() {
        return  _prefs.getString(PACKAGE_NAME + KEY_SCHEDULER_TO, "12:00 AM");
    }

    public void setKEY_SCHEDULER_FROM(String KEY_SCHEDULER_FROM) {
        _editor.putString(PACKAGE_NAME + this.KEY_SCHEDULER_FROM, KEY_SCHEDULER_FROM);
        _editor.commit();
    }

    public String getKEY_SCHEDULER_FROM() {
        return  _prefs.getString(PACKAGE_NAME + KEY_SCHEDULER_FROM, "12:00 AM");
    }

    public void setKEY_SCHEDULER_SUNDAY(boolean KEY_SCHEDULER_SUNDAY) {
        _editor.putBoolean(PACKAGE_NAME + this.KEY_SCHEDULER_SUNDAY, KEY_SCHEDULER_SUNDAY);
        _editor.commit();
    }

    public boolean getKEY_SCHEDULER_SUNDAY() {
        return  _prefs.getBoolean(PACKAGE_NAME + KEY_SCHEDULER_SUNDAY, true);
    }

    public void setKEY_SCHEDULER_MONDAY(boolean KEY_SCHEDULER_MONDAY) {
        _editor.putBoolean(PACKAGE_NAME + this.KEY_SCHEDULER_MONDAY, KEY_SCHEDULER_MONDAY);
        _editor.commit();
    }

    public boolean getKEY_SCHEDULER_MONDAY() {
        return  _prefs.getBoolean(PACKAGE_NAME + KEY_SCHEDULER_MONDAY, true);
    }


    public void setKEY_SCHEDULER_TUESDAY(boolean KEY_SCHEDULER_TUESDAY) {
        _editor.putBoolean(PACKAGE_NAME + this.KEY_SCHEDULER_TUESDAY, KEY_SCHEDULER_TUESDAY);
        _editor.commit();
    }

    public boolean getKEY_SCHEDULER_TUESDAY() {
        return  _prefs.getBoolean(PACKAGE_NAME + KEY_SCHEDULER_TUESDAY, true);
    }


    public void setKEY_SCHEDULER_WEDNESDAY(boolean KEY_SCHEDULER_WEDNESDAY) {
        _editor.putBoolean(PACKAGE_NAME + this.KEY_SCHEDULER_WEDNESDAY, KEY_SCHEDULER_WEDNESDAY);
        _editor.commit();
    }

    public boolean getKEY_SCHEDULER_WEDNESDAY() {
        return  _prefs.getBoolean(PACKAGE_NAME + KEY_SCHEDULER_WEDNESDAY, true);
    }


    public void setKEY_SCHEDULER_THURSDAY(boolean KEY_SCHEDULER_THURSDAY) {
        _editor.putBoolean(PACKAGE_NAME + this.KEY_SCHEDULER_THURSDAY, KEY_SCHEDULER_THURSDAY);
        _editor.commit();
    }

    public boolean getKEY_SCHEDULER_THURSDAY() {
        return  _prefs.getBoolean(PACKAGE_NAME + KEY_SCHEDULER_THURSDAY, true);
    }


    public void setKEY_SCHEDULER_SATURDAY(boolean KEY_SCHEDULER_SATURDAY) {
        _editor.putBoolean(PACKAGE_NAME + this.KEY_SCHEDULER_SATURDAY, KEY_SCHEDULER_SATURDAY);
        _editor.commit();
    }

    public boolean getKEY_SCHEDULER_SATURDAY() {
        return  _prefs.getBoolean(PACKAGE_NAME + KEY_SCHEDULER_SATURDAY, true);
    }

    public void setKEY_SCHEDULER_FRIDAY(boolean KEY_SCHEDULER_FRIDAY) {
        _editor.putBoolean(PACKAGE_NAME + this.KEY_SCHEDULER_FRIDAY, KEY_SCHEDULER_FRIDAY);
        _editor.commit();
    }

    public boolean getKEY_SCHEDULER_FRIDAY() {
        return  _prefs.getBoolean(PACKAGE_NAME + KEY_SCHEDULER_FRIDAY, true);
    }


    public void setKEY_MESSAGE_SENT_ENABLE(boolean KEY_MESSAGE_SENT_ENABLE) {
        _editor.putBoolean(PACKAGE_NAME + this.KEY_MESSAGE_SENT_ENABLE, KEY_MESSAGE_SENT_ENABLE);
        _editor.commit();
    }

    public boolean getKEY_MESSAGE_SENT_ENABLE() {
        return  _prefs.getBoolean(PACKAGE_NAME + KEY_MESSAGE_SENT_ENABLE, true);
    }

    public void setKEY_MESSAGE(String KEY_MESSAGE) {
        _editor.putString(PACKAGE_NAME + this.KEY_MESSAGE, KEY_MESSAGE);
        _editor.commit();
    }

    public String getKEY_MESSAGE() {
        return  _prefs.getString(PACKAGE_NAME + KEY_MESSAGE, null);
    }

    public void setKEY_CALL_BLOCKER_ENABLE(boolean KEY_CALL_BLOCKER_ENABLE) {
        _editor.putBoolean(PACKAGE_NAME + this.KEY_CALL_BLOCKER_ENABLE, KEY_CALL_BLOCKER_ENABLE);
        _editor.commit();
    }

    public boolean getKEY_CALL_BLOCKER_ENABLE() {
        return  _prefs.getBoolean(PACKAGE_NAME + KEY_CALL_BLOCKER_ENABLE, false);
    }


    public void setKEY_BLOCK_PRIVATE_NUMBERS(boolean KEY_BLOCK_PRIVATE_NUMBERS) {
        _editor.putBoolean(PACKAGE_NAME + this.KEY_BLOCK_PRIVATE_NUMBERS, KEY_BLOCK_PRIVATE_NUMBERS);
        _editor.commit();
    }

    public boolean getKEY_BLOCK_PRIVATE_NUMBERS() {
        return  _prefs.getBoolean(PACKAGE_NAME + KEY_BLOCK_PRIVATE_NUMBERS, false);
    }


    public void setKEY_BLOCK_UNKNOWN_NUMBERS(boolean KEY_BLOCK_UNKNOWN_NUMBERS) {
        _editor.putBoolean(PACKAGE_NAME + this.KEY_BLOCK_UNKNOWN_NUMBERS, KEY_BLOCK_UNKNOWN_NUMBERS);
        _editor.commit();
    }

    public boolean getKEY_BLOCK_UNKNOWN_NUMBERS() {
        return  _prefs.getBoolean(PACKAGE_NAME + KEY_BLOCK_UNKNOWN_NUMBERS, false);
    }


    public void setKEY_BLOCK_ALL_CALLS(boolean KEY_BLOCK_ALL_CALLS) {
        _editor.putBoolean(PACKAGE_NAME + this.KEY_BLOCK_ALL_CALLS, KEY_BLOCK_ALL_CALLS);
        _editor.commit();
    }

    public boolean getKEY_BLOCK_ALL_CALLS() {
        return  _prefs.getBoolean(PACKAGE_NAME + KEY_BLOCK_ALL_CALLS, false);
    }




    public void setKEY_SHOW_NOTIFICATION(boolean KEY_SHOW_NOTIFICATION) {
        _editor.putBoolean(PACKAGE_NAME + this.KEY_SHOW_NOTIFICATION, KEY_SHOW_NOTIFICATION);
        _editor.commit();
    }

    public boolean getKEY_SHOW_NOTIFICATION() {
        return  _prefs.getBoolean(PACKAGE_NAME + KEY_SHOW_NOTIFICATION, true);
    }



    public void setKEY_STATUS_BAR_ICON(boolean KEY_STATUS_BAR_ICON) {
        _editor.putBoolean(PACKAGE_NAME + this.KEY_STATUS_BAR_ICON, KEY_STATUS_BAR_ICON);
        _editor.commit();
    }

    public boolean getKEY_STATUS_BAR_ICON() {
        return  _prefs.getBoolean(PACKAGE_NAME + KEY_STATUS_BAR_ICON, true);
    }
}
