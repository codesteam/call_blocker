package holo.components;

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.holo.callblocker.R;

import java.util.ArrayList;

import base.ad.AdController;
import base.ad.AdSettings;
import base.ad.IAd;
import holo.common.CommonConstants;
import holo.common.CommonSettings;
import holo.common.Debug;
import holo.db.DBHelper;
import holo.model.User;
import holo.sm.SmBlacklist;
import holo.sm.SmIncomingOutgoingBlockLogs;
import holo.sm.SmScheduler;
import holo.sm.SmSettings;
import holo.sm.SmWhiteList;
import holo.util.ConnectionDetector;
import holo.util.IAdView;
import holo.util.Print;
import holo.util.ToastMsg;
import materialdesign.activity.FragmentDrawer;
import materialdesign.model.Divider;
import materialdesign.model.NavDrawerItem;


public class MainActivity extends ActionBarActivity implements IAdView, IAd {

    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    private ArrayList<NavDrawerItem> drawerItems;
    private AdController adController;
    LinearLayout containerBoddy;
    LinearLayout llCallBlockerEnable;

    TextView toolBarHeader;
    android.support.v7.widget.SwitchCompat sbToolBarCallBlocker;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        dbHelper = new DBHelper(this);
        toolBarHeader = (TextView) mToolbar.findViewById(R.id.tvHeader);
        sbToolBarCallBlocker = ( android.support.v7.widget.SwitchCompat) mToolbar.findViewById(R.id.sbCalls);

        sbToolBarCallBlocker.setOnTouchListener(null);
        drawerItems = new ArrayList<NavDrawerItem>();
        adController = AdSettings.getInstance(this).createCont(this, getString(R.string.ad_banner_unit_id),
                getString(R.string.ad_inter_unit_id), CommonConstants.AD_WAIT_IN_SEC, CommonConstants.MIN_REQUEST_COUNTER_LIMITE);
        if (Debug.showAd){
            adController.loadInterstitial();

            if (Debug.testingAd){
                adController.setStateTesting(true);
            }
            adController.setStateLoadFirstThenDisplay(true);
        }




        containerBoddy = (LinearLayout) findViewById(R.id.container_body);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);

        drawerItems = new ArrayList<>();
        drawerItems.add(new SmBlacklist(MainActivity.this, true, "Blacklist", "Block them up",
                R.drawable.ic_blacklist, 20, Color.WHITE, R.id.container_body,
                new Divider(null, 0, 0, getResources().getColor(R.color.Gray), 1, 1)));
        drawerItems.add(new SmWhiteList(MainActivity.this, true, "Whitelist","Never blocked numbers",
                R.drawable.ic_whitelist_main, 20, Color.WHITE, R.id.container_body,
                new Divider(null, 0, 0, getResources().getColor(R.color.Gray), 1, 1)));
        drawerItems.add(new SmIncomingOutgoingBlockLogs(MainActivity.this, true, "Block Logs","Rejected calls list",
                R.drawable.ic_block_logs, 20, Color.WHITE, R.id.container_body,
                new Divider(null, 0, 0, getResources().getColor(R.color.Gray), 1, 1)));
        drawerItems.add(new SmScheduler(MainActivity.this, true, "Scheduler", "Make blocker more handy",
                R.drawable.ic_scheduler, 20, Color.WHITE, R.id.container_body,
                new Divider(null, 0, 0, getResources().getColor(R.color.Gray), 1, 1)));
        drawerItems.add(new SmSettings(MainActivity.this, true, "Settings", "Set blocking controls",
                R.drawable.ic_settings_main, 20, Color.WHITE, R.id.container_body,
                new Divider(null, 0, 0, getResources().getColor(R.color.Gray), 1, 1)));



        drawerFragment.initAdapter(drawerItems, R.drawable.selector_list_row);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.profilerImage(CommonConstants.SHOW_PROFILER_IMAGE, null);


        Bundle extras = getIntent().getExtras();
        boolean fromNotification = false;

        if (extras != null) {
            fromNotification = extras.getBoolean("from_notification");
        }

        if (fromNotification){
            if (drawerItems.size() > 2){
                drawerItems.get(2).fire();
            }
        }else{

            if (drawerItems.size() > 0){
                drawerItems.get(0).fire();
            }
        }
        sbToolBarCallBlocker.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                CommonSettings.getInstance().setKEY_CALL_BLOCKER_ENABLE(isChecked);
                Print.e(this, isChecked);
                if (isChecked) {
                    if (CommonSettings.getInstance().getKEY_STATUS_BAR_ICON()){
                        callBlockerActiveNotification();
                    }
                    ToastMsg.Toast(MainActivity.this, R.string.call_blocking_is_on);
                } else {
                    cancelBlockerActiveNotification();
                    ToastMsg.Toast(MainActivity.this, R.string.call_blocking_is_off);
                }
            }
        });


        if (CommonSettings.getInstance().getKEY_STATUS_BAR_ICON() && CommonSettings.getInstance().getKEY_CALL_BLOCKER_ENABLE()){
            callBlockerActiveNotification();
        }
        /*llCallBlockerEnable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwitchButton sb = (SwitchButton) v.findViewById(R.id.sbCallBlocker);
                boolean isChecked = !sb.isChecked();
                sb.setChecked(isChecked);
                CommonSettings.getInstance().setKEY_CALL_BLOCKER_ENABLE(isChecked);
                Print.e(this, " isChecked:" + isChecked);

            }
        });*/
        sbToolBarCallBlocker.setChecked(CommonSettings.getInstance().getKEY_CALL_BLOCKER_ENABLE());

        toolBarHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawerFragment.isVisible()) {
                    drawerFragment.openDrawer();
                } else {
                    drawerFragment.hideDrawer();
                }

            }
        });

        drawerFragment.openDrawer();

        LinearLayout llContainerPassword = (LinearLayout) findViewById(R.id.container_password);

        ArrayList<User> user = dbHelper.getUser();
        if (user.size() > 0){
            llContainerPassword.setVisibility(View.VISIBLE);
            userValidation(user.get(0), llContainerPassword);
        }else{
            llContainerPassword.setVisibility(View.GONE);
        }


        setUpBannerAddview();
    }

    public void userValidation(final User user, final LinearLayout llContainer){

        final EditText etInputPass = (EditText) llContainer.findViewById(R.id.etInputPassword);

        llContainer.findViewById(R.id.btnEnter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pass = etInputPass.getText().toString().trim();
                if (!pass.equals(user.getPassword())){
                    ToastMsg.Toast(MainActivity.this, "Wrong password!!!,\nPlease input correct password");
                }else{
                    llContainer.setVisibility(View.GONE);
                }
            }

        });

        llContainer.findViewById(R.id.btnForgotPass).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new ShowAlert(user).show();
            }
        });



    }


    public void setHeaderText(String text){
        this.toolBarHeader.setText(text);
    }

    @Override
    public void setBannerAd() {
        setUpBannerAddview();
    }



    public void setCallBlockerState(boolean state){
        sbToolBarCallBlocker.setChecked(state);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // AppRater.app_launched(this);
        Print.e(this, "onResume");

        adController.resume();



    }


    @Override
    protected void onPause() {
        super.onPause();
        Print.e(this, "pause");
        adController.pause();
    }


    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        Print.e(this, "On back press");

        if (getSupportFragmentManager().getBackStackEntryCount() == 0){
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }else{
            android.support.v4.app.FragmentManager fm =getSupportFragmentManager();
            fm.popBackStack();
        }

    }


    @Override
    public void showInterAd() {
        // TODO Auto-generated method stub
        Print.e(this, "Show In add");
        if (Debug.showAd) {
            adController.displayInterstitial();
        }

    }

    @Override
    public void onDestroy() {
        adController.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        Print.e(this, "onStop");
        super.onStop();
    }

    public void setUpBannerAddview() {
        if (Debug.showAd) {
            if (ConnectionDetector.isNetworkPresent(this)) {
                adController.setUpBannerAddview((LinearLayout) findViewById(R.id.llAdView));
                setBannerVisibility(View.VISIBLE);
            } else {
                setBannerVisibility(View.GONE);
            }
        }

    }


    public void setBannerVisibility(int v) {
        if (Debug.showAd) {
            adController.setBannerVisibility(v);
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }


    public class ShowAlert extends Dialog {
        User user;


        public ShowAlert(User user) {
            super(MainActivity.this, R.style.PauseDialog);
           this.user = user;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.forgot_password_alert);

            ((TextView) findViewById(R.id.item)).setText("Password hint: "+ user.getHint());
            final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            emailIntent.setType("text/plain");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{user.getEmail()});
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Call Blocker");
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, user.getPassword()+"");


            emailIntent.setType("message/rfc822");



            findViewById(R.id.btnYes).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (user.getEmail() != null && user.getEmail().length() >0){
                        try {
                            startActivity(Intent.createChooser(emailIntent,
                                    "Send email using..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(MainActivity.this,
                                    "No email clients installed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        ToastMsg.Toast(MainActivity.this, "No email address found");
                    }


                    ShowAlert.this.dismiss();
                }
            });

        }

    }

    private  static final int NOTIF_ID = 99999;
    public void callBlockerActiveNotification(){
        NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_blocker_active_status_bar) // notification icon
                .setContentTitle("Calls Blocker") // title for notification
                .setContentText("Blocking is enabled") // message for notification
                .setAutoCancel(false) // clear notification after click
                .setOngoing(true);
        Intent intent = new Intent(this, MainActivity.class);

        PendingIntent pi = PendingIntent.getActivity(this,0,intent,Intent.FLAG_ACTIVITY_NEW_TASK);
        mBuilder.setContentIntent(pi);
        NotificationManager mNotificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIF_ID, mBuilder.build());
    }


    public  void cancelBlockerActiveNotification(){
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(ns);
        nMgr.cancel(NOTIF_ID);
    }


}
