package holo.components;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.support.v7.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;

import com.android.internal.telephony.ITelephony;
import com.holo.callblocker.R;

import java.lang.reflect.Method;
import java.util.ArrayList;

import holo.common.CommonSettings;
import holo.db.DBHelper;
import holo.model.PhoneNumber;
import holo.util.Print;
import holo.util.SchedulerFiltter;

/**
 * Created by water on 4/17/16.
 */
public class PhoneCallStateListener extends PhoneStateListener {

    private Context context;
    private int state;

    public PhoneCallStateListener(Context context) {
        this.context = context;
    }


    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        switch (state) {

            case TelephonyManager.CALL_STATE_RINGING:
                state = 1;
                // String block_number = prefs.getString("block_number", null);

                AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                //Turn ON the mute
                audioManager.setStreamMute(AudioManager.STREAM_RING, true);
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                try {
                    //Toast.makeText(context, "in" + block_number, Toast.LENGTH_LONG).show();
                    Class clazz = Class.forName(telephonyManager.getClass().getName());
                    Method method = clazz.getDeclaredMethod("getITelephony");
                    method.setAccessible(true);
                    ITelephony telephonyService = (ITelephony) method.invoke(telephonyManager);
                    //Checking incoming call number
                    // System.out.println("Call " + block_number);
                    Print.e(this, CommonSettings.getInstance().getKEY_CALL_BLOCKER_ENABLE());
                    if (CommonSettings.getInstance().getKEY_CALL_BLOCKER_ENABLE()) {
                        checkForBlockingNumber(telephonyService, incomingNumber);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                //Turn OFF the mute
                audioManager.setStreamMute(AudioManager.STREAM_RING, false);
                break;

            case TelephonyManager.CALL_STATE_OFFHOOK:
                IncomingCallReceiver.NEW_CALL = false;

        }
        super.onCallStateChanged(state, incomingNumber);
    }

    private void checkForBlockingNumber(ITelephony telephonyService, String incomingNumber) {
        try {

            Print.e(this, "checking for checkForBlockingNumber");
            SchedulerFiltter sf = new SchedulerFiltter();
            if (sf.isShedulerOn()) {
                Print.e(this, "Sceduler is on");
                if (sf.cheekSchedule()) {
                    checkForBlock(telephonyService, incomingNumber);
                }
            } else {
                Print.e(this, "Sceduler is off");
                checkForBlock(telephonyService, incomingNumber);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkForBlock(ITelephony telephonyService, String incomingNumber) {

        Print.e(this, "getKEY_BLOCK_ALL_CALLS: " + CommonSettings.getInstance().getKEY_BLOCK_ALL_CALLS());
        Print.e(this, "getKEY_BLOCK_UNKNOWN_NUMBERS: " + CommonSettings.getInstance().getKEY_BLOCK_UNKNOWN_NUMBERS());
        Print.e(this, "getKEY_BLOCK_PRIVATE_NUMBERS: " + CommonSettings.getInstance().getKEY_BLOCK_PRIVATE_NUMBERS());
        DBHelper dbHelper = new DBHelper(context);
        ArrayList<PhoneNumber> phoneNumbers = dbHelper.getBlackNumbers();

        if (CommonSettings.getInstance().getKEY_BLOCK_ALL_CALLS()) {
            PhoneNumber pn = null;
            for (int i = 0; i < phoneNumbers.size(); i++) {
                PhoneNumber phoneNumber = phoneNumbers.get(i);
                if (incomingNumber.contains(phoneNumber.getPhone() + "")) {
                    pn = phoneNumber;
                    break;
                }
            }

            if (pn == null) {
                String name = getContactDisplayNameByNumber(incomingNumber) + "";
                if (name != null) {
                    pn = new PhoneNumber(incomingNumber, name + "");
                } else {
                    pn = new PhoneNumber(incomingNumber, "Unknown Number");
                }
            }
            blockTheCall(telephonyService, pn, dbHelper);
            return;
        }

        if (CommonSettings.getInstance().getKEY_BLOCK_UNKNOWN_NUMBERS()) {
            PhoneNumber pn = null;
            if (getCountByNumber(incomingNumber) == 0) {
                pn = new PhoneNumber(incomingNumber, "Unknown Number");
                blockTheCall(telephonyService, pn, dbHelper);
                return;
            }
        }

        if (CommonSettings.getInstance().getKEY_BLOCK_PRIVATE_NUMBERS()) {
            try {
                int value = Integer.parseInt(incomingNumber);
                if (value <= 0) {
                    PhoneNumber phoneNumber = new PhoneNumber(incomingNumber, "Private number");
                    blockTheCall(telephonyService, phoneNumber, dbHelper);
                    return;
                }
            } catch (Exception e) {
            }
        }


        for (int i = 0; i < phoneNumbers.size(); i++) {
            PhoneNumber phoneNumber = phoneNumbers.get(i);
            if (incomingNumber.contains(phoneNumber.getPhone() + "")) {
                if (checkForBlockUntil(phoneNumber)) {
                    blockTheCall(telephonyService, phoneNumber, dbHelper);
                }

                break;
            }
        }
    }

    private void blockTheCall(ITelephony telephonyService, PhoneNumber phoneNumber, DBHelper dbHelper) {
        AudioManager mAudio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        CommonSettings.getInstance().setInt(CommonSettings.getInstance().KEY_USER_RINGER_MODE, mAudio.getRingerMode());
        mAudio.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        // telephonyService = (ITelephony) method.invoke(telephonyManager);
        telephonyService.endCall();
        Print.e(this, "Insert log");
        dbHelper.insertLogs(phoneNumber.getName(), phoneNumber.getPhone(), System.currentTimeMillis());
        mAudio.setRingerMode(CommonSettings.getInstance().getInt(CommonSettings.getInstance().KEY_USER_RINGER_MODE));
        checkForSendSMS(phoneNumber);
        if (CommonSettings.getInstance().getKEY_SHOW_NOTIFICATION()) {
            sendBlockNotification();
        }
        DelayClearCallLog DelayClear = new DelayClearCallLog( phoneNumber.getPhone());
        DelayClear.start();
        //removeDefaultCallHistoryAndNotification(phoneNumber.getPhone());
       // DeleteNumFromCallLog(context.getContentResolver(), phoneNumber.getPhone());
    }

    void checkForSendSMS(PhoneNumber phoneNumber) {
        boolean messageSent = false;
        Print.e(this, "Request for checking send sms");
        if (phoneNumber.getSendMessage() == 1 && phoneNumber.getMessage() != null) {
            Print.e(this, "Request for sending sms local");
            sendSMS(phoneNumber, phoneNumber.getMessage());
            messageSent = true;
        }

        if (messageSent == false) {
            if (CommonSettings.getInstance().getKEY_MESSAGE_SENT_ENABLE() &&
                    CommonSettings.getInstance().getKEY_MESSAGE() != null) {
                Print.e(this, "Request for sending sms global");
                sendSMS(phoneNumber, CommonSettings.getInstance().getKEY_MESSAGE());
            }
        }

    }


    /*public void sendSMS(final PhoneNumber phoneNumber, final String message){
         String ACTION_SMS_SENT = "com.holo.android.apis.os.SMS_SENT_ACTION";
        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        sendSMSNotification("SMS sent to:\n" + phoneNumber.getName() + "\n" +"(Message: "+ message+")");
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        sendSMSNotification("SMS sending failed:\n"+phoneNumber.getName());
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        sendSMSNotification("SMS sending failed:\n"+phoneNumber.getName());
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        sendSMSNotification("SMS sending failed:\n"+phoneNumber.getName());
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        sendSMSNotification("SMS sending failed:\n"+phoneNumber.getName());
                        break;
                }


            }
        }, new IntentFilter(ACTION_SMS_SENT));

        //sms recipient added by user from the activity screen
        SmsManager sms = SmsManager.getDefault();
        List<String> messages = sms.divideMessage(message);
        for (String msg : messages) {
            sms.sendTextMessage(phoneNumber.getPhone(), null, msg, PendingIntent.getBroadcast(
                    context, 0, new Intent(ACTION_SMS_SENT), 0), null);
        }
    }*/
    public void sendSMS(final PhoneNumber phoneNumber, String message) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNumber.getPhone(), null, message, null, null);
            sendSMSNotification("SMS sent to:\n" + phoneNumber.getName() + "\n" + "(Message: " + message + ")");
        } catch (Exception ex) {
            ex.printStackTrace();
            sendSMSNotification("SMS sending failed:\n" + phoneNumber.getName() + "\n" + phoneNumber.getPhone());
        }
    }

    boolean checkForBlockUntil(PhoneNumber phoneNumber) {
        if (phoneNumber.getBlockUntile() == 1) {
            if (phoneNumber.getBlockUntilTime() > System.currentTimeMillis()) {
                return true; // Block untile is enable and it's in time, means it not time out yet
            } else {
                return false;
            }
        } else {
            return true; // no need to check cause BlockUntile is disable
        }
    }

    public void sendBlockNotification() {
        NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_status_bar_notification) // notification icon
                .setContentTitle("Calls Blocker") // title for notification
                .setContentText("Click to view log") // message for notification
                .setAutoCancel(true); // clear notification after click
        Intent intent = new Intent(context, MainActivity.class);
        Bundle bundle = new Bundle();
        // bundle.putBoolean();
        intent.putExtra("from_notification", true);

        PendingIntent pi = PendingIntent.getActivity(context, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
        mBuilder.setContentIntent(pi);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(7777, mBuilder.build());
    }


    public void sendSMSNotification(String message) {
        NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_blocker_active_status_bar) // notification icon
                .setContentTitle("Calls Blocker") // title for notification
                .setContentText(message) // message for notification
                .setAutoCancel(true); // clear notification after click
        Intent intent = new Intent(context, MainActivity.class);
        Bundle bundle = new Bundle();
        // bundle.putBoolean();
        intent.putExtra("from_notification", true);

        PendingIntent pi = PendingIntent.getActivity(context, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
        mBuilder.setContentIntent(pi);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(8888, mBuilder.build());
    }

    public String getContactDisplayNameByNumber(String number) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        String name = null;

        ContentResolver contentResolver = context.getContentResolver();
        Cursor contactLookup = contentResolver.query(uri, new String[]{BaseColumns._ID,
                ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);

        try {
            if (contactLookup != null && contactLookup.getCount() > 0) {
                contactLookup.moveToNext();
                name = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
                //String contactId = contactLookup.getString(contactLookup.getColumnIndex(BaseColumns._ID));
            }
        } finally {
            if (contactLookup != null) {
                contactLookup.close();
            }
        }

        return name;
    }

    public int getCountByNumber(String number) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        ContentResolver contentResolver = context.getContentResolver();
        Cursor contactLookup = contentResolver.query(uri, new String[]{BaseColumns._ID,
                ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);

        try {
            if (contactLookup != null && contactLookup.getCount() > 0) {
                return 1;
            }
        } finally {
            if (contactLookup != null) {
                contactLookup.close();
            }
        }

        return 0;
    }
    /*public void DeleteNumFromCallLog(ContentResolver resolver, String strNum)
    {
        Print.e(this, "DeleteNumFromCallLog: "+ strNum);
        try
        {
            String strUriCalls = "content://call_log/calls";
            Uri UriCalls = Uri.parse(strUriCalls);
            //Cursor c = res.query(UriCalls, null, null, null, null);
            if(null != resolver)
            {
                resolver.delete(UriCalls, CallLog.Calls.NUMBER + "=?", new String[]{strNum});
            }
        }
        catch(Exception e)
        {
            e.getMessage();
        }
    }*/
    private void removeCallHistoryByNumber(String numberTag) {
        Uri UriCalls = Uri.parse("content://call_log/calls");
        Cursor cursor = context.getApplicationContext().getContentResolver().query(UriCalls, null, null, null, null);

        String queryString= "NUMBER='" + numberTag + "'";
        if (cursor.getCount() > 0){
            context.getApplicationContext().getContentResolver().delete(UriCalls, queryString, null);
        }
    }

    private void removeCallHistoryByName(String numberTag) {
        Uri UriCalls = Uri.parse("content://call_log/calls");
        Cursor cursor = context.getApplicationContext().getContentResolver().query(UriCalls, null, null, null, null);

        String queryString= "NAME='" + numberTag + "'";
        if (cursor.getCount() > 0){
           context.getApplicationContext().getContentResolver().delete(UriCalls, queryString, null);
        }
    }

    public class DelayClearCallLog extends Thread {

        public String phoneNumber;

        public DelayClearCallLog( String pNumber){

            phoneNumber = pNumber;
        }

        public void run() {
            try {
                sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            clearCallLog( phoneNumber);
        }
        public void clearCallLog(String phoneNumber) {
            // implement delete query here
            String name = getContactDisplayNameByNumber(phoneNumber);
            if (name != null){ // bypass solution to avoid area code problem,
                removeCallHistoryByName(name);
            }else{
                removeCallHistoryByNumber(phoneNumber);
            }

        }
    }
}

