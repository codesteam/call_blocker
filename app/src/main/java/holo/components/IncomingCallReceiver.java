package holo.components;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.android.internal.telephony.ITelephony;

import java.lang.reflect.Method;

import holo.common.CommonSettings;

/**
 * Created by water on 4/17/16.
 */
public class IncomingCallReceiver  extends BroadcastReceiver
{
    public static boolean NEW_CALL = true;
    @Override
    public void onReceive(Context context, Intent intent)
    {

        // If, the received action is not a type of "Phone_State", ignore it
        if (!intent.getAction().equals("android.intent.action.PHONE_STATE"))
            return;
            // Else, try to do some action
        else
        {
            if (NEW_CALL){
                TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                PhoneCallStateListener customPhoneListener = new PhoneCallStateListener(context);
                telephony.listen(customPhoneListener, PhoneStateListener.LISTEN_CALL_STATE);
                NEW_CALL = false;
            }


        }

    }

    // Method to disconnect phone automatically and programmatically
    // Keep this method as it is
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void disconnectPhoneItelephony(Context context)
    {
        AudioManager mAudio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        CommonSettings.getInstance().setInt(CommonSettings.getInstance().KEY_USER_RINGER_MODE, mAudio.getRingerMode());
        mAudio.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        ITelephony telephonyService;
        TelephonyManager telephony = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        try
        {
            Class c = Class.forName(telephony.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            telephonyService = (ITelephony) m.invoke(telephony);
            telephonyService.endCall();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        mAudio.setRingerMode(CommonSettings.getInstance().getInt(CommonSettings.getInstance().KEY_USER_RINGER_MODE));
       // audioManager.setStreamMute(AudioManager.STREAM_RING, false);

    }
}
