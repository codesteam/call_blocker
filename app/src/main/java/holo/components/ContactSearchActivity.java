package holo.components;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.holo.callblocker.R;

import java.util.ArrayList;

import base.ad.AdController;
import base.ad.AdSettings;
import base.ad.IAd;
import holo.adapter.ContactListAdapter;
import holo.common.Debug;
import holo.db.DBHelper;
import holo.model.OrmContact;
import holo.util.ConnectionDetector;
import holo.util.IAdView;
import holo.util.Print;


public class ContactSearchActivity extends ActionBarActivity implements IAdView, IAd {

    private Toolbar mToolbar;
    private AdController adController;
    ContactListAdapter contactListAdapter;
    public static final int CONTACT_LOADER_ID = 78; // From docs: A unique identifier for this loader. Can be whatever you want.
    // Defines the asynchronous callback for the contacts data loader
    ArrayList<OrmContact> ormContacts;
    ArrayList<OrmContact> catContactList; // cat 0 for blacklist cat 1 for whitelist
    ListView listView;
    EditText inputSearch;
    DBHelper dbHelper;
    int cat;
    FloatingActionButton fabDone;
    private LoaderManager.LoaderCallbacks<Cursor> contactsLoader =
            new LoaderManager.LoaderCallbacks<Cursor>() {
                // Create and return the actual cursor loader for the contacts data
                @Override
                public Loader<Cursor> onCreateLoader(int id, Bundle args) {
                    // Define the columns to retrieve
                    String[] projectionFields = new String[]{ContactsContract.Contacts._ID,
                            ContactsContract.Contacts.DISPLAY_NAME,
                            ContactsContract.Contacts.PHOTO_URI
                    };
                    // Construct the loader
                    String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
                            + " COLLATE LOCALIZED ASC";

                    CursorLoader cursorLoader = new CursorLoader(ContactSearchActivity.this,
                            ContactsContract.Contacts.CONTENT_URI, // URI
                            projectionFields, // projection fields
                            null, // the selection criteria
                            null, // the selection args
                            sortOrder // the sort order
                    );
                    // Return the loader for use
                    return cursorLoader;
                }

                // When the system finishes retrieving the Cursor through the CursorLoader,
                // a call to the onLoadFinished() method takes place.
                @Override
                public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
                    // The swapCursor() method assigns the new Cursor to the holoContactlistAdapter
                    //holoContactlistAdapter.swapCursor(cursor);
                    ormContacts = new ArrayList<>();

                    while (cursor.moveToNext()) {
                        String refId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                        String title = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        // Print.e(this, contactId);
                        if (title != null) {
                            OrmContact ormContact = new OrmContact(title, refId, cat);
                            ormContact.setType(2);
                            checkContactSelected(ormContact);
                            ormContacts.add(ormContact);
                        }

                    }
                    contactListAdapter = new ContactListAdapter(ContactSearchActivity.this, ormContacts, cat, fabDone);
                    listView.setAdapter(contactListAdapter);
                    //contactListCursorAdapter.swapCursor(cursor);
                }

                // This method is triggered when the loader is being reset
                // and the loader data is no longer available. Called if the data
                // in the provider changes and the Cursor becomes stale.
                @Override
                public void onLoaderReset(Loader<Cursor> loader) {
                    // Clear the Cursor we were using with another call to the swapCursor()
                    //holoContactlistAdapter.swapCursor(null);
                    // contactListCursorAdapter.swapCursor(null);

                }
            };

    private void checkContactSelected(OrmContact ormContact) {
        for (int j = 0; j < catContactList.size(); j++) {
            if (catContactList.get(j).getName().equals(ormContact.getName())) {
                ormContact.setSelected(true);
                break;
            }
        }
    }

    private void displaySmsLog() {
        Uri allMessages = Uri.parse("content://sms/");
        //Cursor cursor = managedQuery(allMessages, null, null, null, null); Both are same
        Cursor cursor = this.getContentResolver().query(allMessages, null,
                null, null, null);

        while (cursor.moveToNext()) {
            for (int i = 0; i < cursor.getColumnCount(); i++) {
                if (cursor.getString(i) != null)
                    if (cursor.getString(i).equals("Raju Bro Act"))
                        Log.e(cursor.getColumnName(i) + "", cursor.getString(i) + "" + i);
            }

        }

    }

    public void loadCallLog() {

        String[] callLogFields = {android.provider.CallLog.Calls._ID,
                android.provider.CallLog.Calls.NUMBER,
                android.provider.CallLog.Calls.CACHED_NAME /* im not using the name but you can*/};
        String viaOrder = android.provider.CallLog.Calls.DATE + " DESC";
        String WHERE = android.provider.CallLog.Calls.NUMBER + " >0"; /*filter out private/unknown numbers */

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        final Cursor cursor = this.getContentResolver().query(
                android.provider.CallLog.Calls.CONTENT_URI, callLogFields,
                null, null, viaOrder);


        ormContacts = new ArrayList<>();
        ArrayList<String> ids = new ArrayList<>();

        ArrayList<String> names = new ArrayList<>();
        ArrayList<ArrayList<String>> mobileNumbers = new ArrayList<>();
        while (cursor.moveToNext()){
            String id = cursor.getString( cursor.getColumnIndex(android.provider.CallLog.Calls._ID ));
            String title = cursor.getString( cursor.getColumnIndex(android.provider.CallLog.Calls.CACHED_NAME ));
            String mobileNumber = cursor.getString( cursor.getColumnIndex(android.provider.CallLog.Calls.NUMBER));

            //Print.e(this, id);
            boolean dup = false;
            if (title != null){
                /*for (String name:names) {
                    if (name.equals(title)){
                        dup = true;
                        break;
                    }
                }
                if (!dup){
                    names.add(title);
                    mobileNumbers.add(mobileNumber);
                }*/
                if (!names.contains(title)){
                    names.add(title);
                    ids.add(id);
                    ArrayList<String> nums = new ArrayList<>();
                    nums.add(mobileNumber);
                    mobileNumbers.add(nums);
                }

            }else{
                /*for (String name:mobileNumbers) {
                    if (name.equals(mobileNumber)){
                        dup = true;
                        break;
                    }
                }
                if (!dup){
                    names.add(mobileNumber);
                    mobileNumbers.add(mobileNumber);
                }*/

                if (!names.contains(mobileNumber)){
                    names.add(mobileNumber);
                    ids.add(id);
                    ArrayList<String> nums = new ArrayList<>();
                    nums.add(mobileNumber);
                    mobileNumbers.add(nums);
                }
            }

        }

        for (int i = 0; i < names.size(); i++){
            OrmContact ormContact = new OrmContact(names.get(i), ids.get(i), cat);
            ormContact.setType(1);
            checkContactSelected(ormContact);
            ormContact.setNumbers(mobileNumbers.get(i));
            ormContacts.add(ormContact);

        }
        cursor.close();



        contactListAdapter = new ContactListAdapter(ContactSearchActivity.this, ormContacts, cat, fabDone);
        listView.setAdapter(contactListAdapter);


    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_search);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        listView = (ListView) findViewById(R.id.lvContacts);
        inputSearch = (EditText) findViewById(R.id.inputSearch);
        dbHelper = new DBHelper(this);

        String title = "";
        Bundle bundle = getIntent().getExtras();
        String option = bundle.getString("search_option");
        cat = bundle.getInt("cat"); // 0 for blacklist 1 for whitelist
        catContactList = dbHelper.getHoloContactList(cat);
        fabDone = (FloatingActionButton)findViewById(R.id.fabDone);
        fabDone.setVisibility(View.INVISIBLE);
        fabDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactSearchActivity.this.finish();
            }
        });

        //LinearLayout llInputView = (LinearLayout) findViewById(R.id.inputView);
       // LinearLayout llContactSearchView = (LinearLayout) findViewById(R.id.contactSerchView);
        if (option.equals("from_calls_log")){
            title = "Calls log";
            //llContactSearchView.setVisibility(View.VISIBLE);
           // llInputView.setVisibility(View.GONE);
            loadCallLog();
        }else if(option.equals("from_contact")){
            title = "Contact";
            //llContactSearchView.setVisibility(View.VISIBLE);
            //llInputView.setVisibility(View.GONE);
            getSupportLoaderManager().initLoader(CONTACT_LOADER_ID,
                    new Bundle(), contactsLoader);
        }else if(option.equals("input_number")){
            title = "Input";
            //llContactSearchView.setVisibility(View.GONE);
            //llInputView.setVisibility(View.VISIBLE);
        }
        ((TextView) mToolbar.findViewById(R.id.tvTitle)).setText(title);
        mToolbar.findViewById(R.id.toolbarBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactSearchActivity.this.finish();

            }
        });

        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                ContactSearchActivity.this.contactListAdapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
        adController = AdSettings.getInstance(this).getAdController(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        setUpBannerAddview();
    }


    @Override
    public void setBannerAd() {
        setUpBannerAddview();
    }




    @Override
    protected void onResume() {
        super.onResume();
       // AppRater.app_launched(this);
        Print.e(this, "onResume");
        adController.resume();



    }


    @Override
    protected void onPause() {
        super.onPause();
        Print.e(this, "pause");
        adController.pause();

    }

    @Override
    public void showInterAd() {
        // TODO Auto-generated method stub
        Print.e(this, "Show In add");
        if (Debug.showAd) {
            adController.displayInterstitial();
        }

    }

    @Override
    public void onDestroy() {
        adController.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        Print.e(this, "onStop");
        super.onStop();
    }

    public void setUpBannerAddview() {
        if (Debug.showAd) {
            if (ConnectionDetector.isNetworkPresent(this)) {
                adController.setUpBannerAddview((LinearLayout) findViewById(R.id.llAdView));
                setBannerVisibility(View.VISIBLE);
            } else {
                setBannerVisibility(View.GONE);
            }
        }

    }


    public void setBannerVisibility(int v) {
        if (Debug.showAd) {
            adController.setBannerVisibility(v);
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }




}
