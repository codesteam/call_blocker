package holo.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.holo.callblocker.R;

import holo.common.CommonSettings;
import holo.components.MainActivity;

/**
 * Created by water on 4/23/16.
 */
public class BlockNotification extends Fragment {
    LinearLayout llToolBarBack;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.settings_notification, container, false);
        llToolBarBack = (LinearLayout)rootView.findViewById(R.id.toolbarBack);
        final SwitchCompat swShowNotification = (SwitchCompat) rootView.findViewById(R.id.swShowNotification);
        final SwitchCompat swStatusBarIcon = (SwitchCompat) rootView.findViewById(R.id.swStatusBarIcon);

        swShowNotification.setChecked(CommonSettings.getInstance().getKEY_SHOW_NOTIFICATION());
        swStatusBarIcon.setChecked(CommonSettings.getInstance().getKEY_STATUS_BAR_ICON());

        rootView.findViewById(R.id.llShowNotification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = !swShowNotification.isChecked();
                swShowNotification.setChecked(isChecked);
                CommonSettings.getInstance().setKEY_SHOW_NOTIFICATION(isChecked);
            }
        });


        rootView.findViewById(R.id.llStatusBarIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = !swStatusBarIcon.isChecked();
                if(!isChecked){
                    new ShowAlert(swStatusBarIcon, isChecked).show();
                }else{
                    swStatusBarIcon.setChecked(isChecked);
                    CommonSettings.getInstance().setKEY_STATUS_BAR_ICON(isChecked);
                    if(CommonSettings.getInstance().getKEY_CALL_BLOCKER_ENABLE()){
                        ((MainActivity)BlockNotification.this.getActivity()).callBlockerActiveNotification();
                    }
                }

            }
        });


        llToolBarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.FragmentManager fm = BlockNotification.this.getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((TextView)llToolBarBack.findViewById(R.id.tvTitle)).setText("Notifications");
        ((MainActivity)this.getActivity()).showInterAd();


    }


    public class ShowAlert extends Dialog {
        SwitchCompat switchCompat;
        boolean status;
        public ShowAlert(SwitchCompat switchCompat, boolean status) {
            super(BlockNotification.this.getActivity(), R.style.PauseDialog);
            this.switchCompat = switchCompat;
            this.status = status;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.status_bar_notification_alart);


            findViewById(R.id.btnYes).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switchCompat.setChecked(status);
                    CommonSettings.getInstance().setKEY_STATUS_BAR_ICON(status);
                    ((MainActivity)BlockNotification.this.getActivity()).cancelBlockerActiveNotification();

                    ShowAlert.this.dismiss();

                }
            });

            findViewById(R.id.btnNo).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShowAlert.this.dismiss();

                }
            });
        }

    }
}

