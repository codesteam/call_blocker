package holo.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.holo.callblocker.R;

import java.util.ArrayList;

import holo.adapter.BlacklistLogsAdapter;
import holo.components.MainActivity;
import holo.db.DBHelper;
import holo.model.BlockCallLog;


/**
 * Created by water on 2/19/16.
 */
public class BlacklistLogs extends Fragment {

    private FloatingActionButton fab;
    BlacklistLogsAdapter adapter;
    ListView listView;
    DBHelper dbHelper;
    TextView tvHint;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.black_list_logs, container, false);
        fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        listView = (ListView) rootView.findViewById(R.id.listView);
        tvHint = (TextView) rootView.findViewById(R.id.hint);
        dbHelper = new DBHelper(this.getActivity());
        ArrayList<BlockCallLog> blockCallLogs = dbHelper.getBlockLogs();
        setTvHintVisibility(blockCallLogs);

        if(blockCallLogs.size() > 0){
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new DeleteAll(BlacklistLogs.this.getActivity(), dbHelper).show();
                }
            });
        }

        adapter = new BlacklistLogsAdapter(this,  blockCallLogs);
        listView.setAdapter(adapter);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ArrayList<BlockCallLog> blockCallLogs = dbHelper.getBlockLogs();
        setTvHintVisibility(blockCallLogs);
        adapter.resetAdapter(blockCallLogs);
        ((MainActivity)this.getActivity()).setHeaderText("Blacklist Logs");
        ((MainActivity)this.getActivity()).showInterAd();
    }

    public void setTvHintVisibility(ArrayList<BlockCallLog> blacklistContacts){
        if (blacklistContacts != null && blacklistContacts.size() > 0){
            tvHint.setVisibility(View.GONE);
        }else{
            tvHint.setVisibility(View.VISIBLE);
        }
    }

    public class DeleteAll extends Dialog {

        Activity context;
        DBHelper dbHelper;
        public DeleteAll(Activity context, DBHelper dbHelper) {
            super(context, R.style.PauseDialog);
            this.context = context;
            this.dbHelper = dbHelper;

        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.delete_all);

            findViewById(R.id.btnYes).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dbHelper.deleteBlockLogs();
                    ArrayList<BlockCallLog> blockCallLogs = dbHelper.getBlockLogs();
                    setTvHintVisibility(blockCallLogs);
                    adapter.resetAdapter(blockCallLogs);
                    DeleteAll.this.dismiss();

                }
            });

            findViewById(R.id.btnNo).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DeleteAll.this.dismiss();

                }
            });
        }

    }

}

