package holo.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.holo.callblocker.R;

import holo.common.CommonSettings;
import holo.components.MainActivity;

/**
 * Created by water on 4/23/16.
 */
public class BlockCalls extends Fragment {
    //android.support.v7.widget.SwitchCompat swCalls;
    LinearLayout llToolBarBack;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.settings_calls, container, false);
        final SwitchCompat swCalls = ( android.support.v7.widget.SwitchCompat) rootView.findViewById(R.id.swCalls);
       final ScrollView svViewArea = (ScrollView) rootView.findViewById(R.id.svCalls);
        llToolBarBack = (LinearLayout)rootView.findViewById(R.id.toolbarBack);

       final AppCompatCheckBox cbPrivateNumber = (AppCompatCheckBox) rootView.findViewById(R.id.cbBlockPrivateNumbers);
       final AppCompatCheckBox cbUnknownNumber = (AppCompatCheckBox) rootView.findViewById(R.id.cbBlockUnknownNumbers);
       final AppCompatCheckBox cbAllcalls = (AppCompatCheckBox) rootView.findViewById(R.id.cbBlockAllCalls);

        llToolBarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.FragmentManager fm =BlockCalls.this.getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });

        swCalls.setChecked(CommonSettings.getInstance().getKEY_CALL_BLOCKER_ENABLE());
        disableEnableControls(CommonSettings.getInstance().getKEY_CALL_BLOCKER_ENABLE(), svViewArea);

        rootView.findViewById(R.id.rlBlockCallsEnable).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = !swCalls.isChecked();
                swCalls.setChecked(isChecked);
                CommonSettings.getInstance().setKEY_CALL_BLOCKER_ENABLE(isChecked);
                ((MainActivity) BlockCalls.this.getActivity()).setCallBlockerState(isChecked);
                disableEnableControls(isChecked, svViewArea);

            }
        });

        cbPrivateNumber.setChecked(CommonSettings.getInstance().getKEY_BLOCK_PRIVATE_NUMBERS());
        cbUnknownNumber.setChecked(CommonSettings.getInstance().getKEY_BLOCK_UNKNOWN_NUMBERS());
        cbAllcalls.setChecked(CommonSettings.getInstance().getKEY_BLOCK_ALL_CALLS());

        rootView.findViewById(R.id.llBlockPrivateNumbers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = !cbPrivateNumber.isChecked();
                cbPrivateNumber.setChecked(isChecked);
                CommonSettings.getInstance().setKEY_BLOCK_PRIVATE_NUMBERS(isChecked);

            }
        });

        rootView.findViewById(R.id.llBlockUnknownNumbers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = !cbUnknownNumber.isChecked();
                cbUnknownNumber.setChecked(isChecked);
                CommonSettings.getInstance().setKEY_BLOCK_UNKNOWN_NUMBERS(isChecked);

            }
        });

        rootView.findViewById(R.id.llBlockAllCalls).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = !cbAllcalls.isChecked();
                cbAllcalls.setChecked(isChecked);
                CommonSettings.getInstance().setKEY_BLOCK_ALL_CALLS(isChecked);

            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((TextView)llToolBarBack.findViewById(R.id.tvTitle)).setText("Calls");
        ((MainActivity)this.getActivity()).showInterAd();

    }

    private void disableEnableControls(boolean enable, ViewGroup vg){
        for (int i = 0; i < vg.getChildCount(); i++){
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup){
                disableEnableControls(enable, (ViewGroup)child);
            }
        }
    }
}

