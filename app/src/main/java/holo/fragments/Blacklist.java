package holo.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.holo.callblocker.R;

import java.util.ArrayList;

import holo.adapter.BlacklistAdapter;
import holo.components.MainActivity;
import holo.db.DBHelper;
import holo.dialog.InputChooserList;
import holo.model.OrmContact;


/**
 * Created by water on 2/19/16.
 */
public class Blacklist extends HoloContactList {

    protected BlacklistAdapter blacklistAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.black_list, container, false);
        cat = 0;
        fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.listView);
        tvHint = (TextView) rootView.findViewById(R.id.hint);



        dbHelper = new DBHelper(this.getActivity());
        blacklistAdapter = new BlacklistAdapter(this, dbHelper.getHoloContactList(cat), cat);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(blacklistAdapter);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new InputChooserList((MainActivity) Blacklist.this.getActivity(), cat, blacklistAdapter, null).show();
            }
        });
        /*recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new FragmentDrawer.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                try {
                    items.get(position).fire();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Log.e(" extends FragmentParent", "Please override fire() function, and call super.fire() if necessary");
                }
                mDrawerLayout.closeDrawer(containerView);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));*/
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ArrayList<OrmContact> ormContacts = dbHelper.getHoloContactList(cat);
        setTvHintVisibility(ormContacts);
        blacklistAdapter.resetAdapter(ormContacts);
        ((MainActivity)this.getActivity()).setHeaderText("Blacklist");
        ((MainActivity)this.getActivity()).showInterAd();


    }

    @Override
    public void setTvHintVisibility(ArrayList<OrmContact> ormContacts) {
        super.setTvHintVisibility(ormContacts);
    }


}

