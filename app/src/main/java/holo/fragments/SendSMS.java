package holo.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.holo.callblocker.R;

import holo.common.CommonSettings;
import holo.components.MainActivity;
import holo.util.ToastMsg;

/**
 * Created by water on 4/23/16.
 */
public class SendSMS extends Fragment {

    LinearLayout llToolBarBack;
    EditText etWriteSMS;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.settings_send_sms, container, false);
        llToolBarBack = (LinearLayout)rootView.findViewById(R.id.toolbarBack);

        final ScrollView svViewArea = (ScrollView) rootView.findViewById(R.id.svSendSMS);
        final SwitchCompat swSendSMS = ( android.support.v7.widget.SwitchCompat) rootView.findViewById(R.id.swSendSMS);
        etWriteSMS = (EditText) rootView.findViewById(R.id.etWriteMessage);
        Button btnSave = (Button) rootView.findViewById(R.id.btnSave);
        Button btnClear = (Button) rootView.findViewById(R.id.btnClear);
        swSendSMS.setChecked(CommonSettings.getInstance().getKEY_MESSAGE_SENT_ENABLE());
        disableEnableControls(CommonSettings.getInstance().getKEY_MESSAGE_SENT_ENABLE(), svViewArea);

        rootView.findViewById(R.id.llSendSMS).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = !swSendSMS.isChecked();
                swSendSMS.setChecked(isChecked);
                CommonSettings.getInstance().setKEY_MESSAGE_SENT_ENABLE(isChecked);
                disableEnableControls(isChecked, svViewArea);

            }
        });


        if (CommonSettings.getInstance().getKEY_MESSAGE() != null){
            etWriteSMS.setText(CommonSettings.getInstance().getKEY_MESSAGE());
        }
        llToolBarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.FragmentManager fm = SendSMS.this.getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = etWriteSMS.getText().toString().trim();

                if (text.length() > 0) {
                    CommonSettings.getInstance().setKEY_MESSAGE(text);
                    ToastMsg.Toast(SendSMS.this.getActivity(), R.string.tst_message_saved_successfully);
                    android.support.v4.app.FragmentManager fm = SendSMS.this.getActivity().getSupportFragmentManager();
                    fm.popBackStack();
                } else {
                    CommonSettings.getInstance().setKEY_MESSAGE(null);
                    ToastMsg.Toast(SendSMS.this.getActivity(), R.string.tst_please_write_message);
                }
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonSettings.getInstance().setKEY_MESSAGE(null);
                ToastMsg.Toast(SendSMS.this.getActivity(), R.string.tst_cleaning_done);
                etWriteSMS.setText("");
            }
        });
        return rootView;
    }

    private void disableEnableControls(boolean enable, ViewGroup vg){
        for (int i = 0; i < vg.getChildCount(); i++){
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup){
                disableEnableControls(enable, (ViewGroup)child);
            }
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        ((TextView)llToolBarBack.findViewById(R.id.tvTitle)).setText("Send SMS");
        ((MainActivity)this.getActivity()).showInterAd();

    }

}

