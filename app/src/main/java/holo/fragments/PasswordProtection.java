package holo.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.holo.callblocker.R;

import holo.components.MainActivity;
import holo.db.DBHelper;
import holo.util.ToastMsg;

/**
 * Created by water on 4/23/16.
 */
public class PasswordProtection extends Fragment {

    LinearLayout llToolBarBack;

    int userCount; // value = 0 means no password protection, value > 0 means password protected
    DBHelper dbHelper;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.settings_password_protection, container, false);
        llToolBarBack = (LinearLayout)rootView.findViewById(R.id.toolbarBack);

        final EditText etPassword = (EditText) rootView.findViewById(R.id.etInputPassword);
        final EditText etConfirmPassword = (EditText) rootView.findViewById(R.id.etConfirmPassword);
        final EditText etHint = (EditText) rootView.findViewById(R.id.etHint);
        final EditText etEmail = (EditText) rootView.findViewById(R.id.etEmailAddress);

        final ScrollView svViewArea = (ScrollView) rootView.findViewById(R.id.svPasswordProtection);
        final SwitchCompat swPassProtection = ( android.support.v7.widget.SwitchCompat) rootView.findViewById(R.id.swPasswordProtection);


        dbHelper = new DBHelper(this.getActivity());
        userCount = dbHelper.getUser().size();
        
        llToolBarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.FragmentManager fm = PasswordProtection.this.getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });

        if (userCount > 0){
            swPassProtection.setChecked(true);
            disableEnableControls(true, svViewArea);
        }else{
            swPassProtection.setChecked(false);
            disableEnableControls(false, svViewArea);
        }

        rootView.findViewById(R.id.llEnablePasswordProtection).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               boolean isChecked = !swPassProtection.isChecked();
                swPassProtection.setChecked(isChecked);
                disableEnableControls(isChecked, svViewArea);
                if (!isChecked){
                    dbHelper.deleteUser();
                }
            }
        });
        rootView.findViewById(R.id.btnSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = etPassword.getText().toString().trim() + "";
                String confirmPass = etConfirmPassword.getText().toString().trim()+ "";
                String hint = etHint.getText().toString().trim()+ "";
                String email = etEmail.getText().toString().trim()+ "";

                if (password.length() <=0){
                    etPassword.requestFocus();
                    InputMethodManager imm = (InputMethodManager) PasswordProtection.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(etPassword, InputMethodManager.SHOW_IMPLICIT);
                    return;
                }

                if (confirmPass.length() <=0){
                    etConfirmPassword.requestFocus();
                    InputMethodManager imm = (InputMethodManager) PasswordProtection.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(etConfirmPassword, InputMethodManager.SHOW_IMPLICIT);
                    return;
                }

                if (hint.length() <=0){
                    etHint.requestFocus();
                    InputMethodManager imm = (InputMethodManager) PasswordProtection.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(etHint, InputMethodManager.SHOW_IMPLICIT);
                    return;
                }

                if (!password.equals(confirmPass)){
                    ToastMsg.Toast(PasswordProtection.this.getActivity(), R.string.tst_password_confirm_pass_dosent_match);
                    return;
                }

                if (userCount > 0){
                    dbHelper.deleteUser();
                }

                dbHelper.insertUser(password, hint, email);
                ToastMsg.Toast(PasswordProtection.this.getActivity(),
                        R.string.tst_user_created_successfully);

                android.support.v4.app.FragmentManager fm = PasswordProtection.this.getActivity().getSupportFragmentManager();
                fm.popBackStack();


            }
        });

        rootView.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.FragmentManager fm = PasswordProtection.this.getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((TextView)llToolBarBack.findViewById(R.id.tvTitle)).setText("Password Protection");
        ((MainActivity)this.getActivity()).showInterAd();

    }

    private void disableEnableControls(boolean enable, ViewGroup vg){
        for (int i = 0; i < vg.getChildCount(); i++){
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup){
                disableEnableControls(enable, (ViewGroup)child);
            }
        }
    }

}

