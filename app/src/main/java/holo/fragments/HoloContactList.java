package holo.fragments;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import holo.db.DBHelper;
import holo.model.OrmContact;

/**
 * Created by water on 4/19/16.
 */
public class HoloContactList extends Fragment{

    protected FloatingActionButton fab;
    protected RecyclerView recyclerView;
    protected DBHelper dbHelper;
    protected TextView tvHint;
    protected int cat = 0;

    public void setTvHintVisibility(ArrayList<OrmContact> ormContacts){
        if (ormContacts != null && ormContacts.size() > 0){
            tvHint.setVisibility(View.GONE);
        }else{
            tvHint.setVisibility(View.VISIBLE);
        }
    }
}
