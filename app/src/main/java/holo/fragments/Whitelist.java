package holo.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.holo.callblocker.R;

import java.util.ArrayList;

import holo.adapter.WhitelistAdapter;
import holo.components.MainActivity;
import holo.db.DBHelper;
import holo.dialog.InputChooserList;
import holo.model.OrmContact;


/**
 * Created by water on 2/19/16.
 */
public class Whitelist extends HoloContactList {


    WhitelistAdapter whitelistAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.white_list, container, false);
        cat = 1;
        fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.listView);
        tvHint = (TextView) rootView.findViewById(R.id.hint);



        dbHelper = new DBHelper(this.getActivity());
        whitelistAdapter = new WhitelistAdapter(this, dbHelper.getHoloContactList(cat), cat);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(whitelistAdapter);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new InputChooserList((MainActivity) Whitelist.this.getActivity(), cat,null, whitelistAdapter).show();
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ArrayList<OrmContact> ormContacts = dbHelper.getHoloContactList(cat);
        setTvHintVisibility(ormContacts);
        whitelistAdapter.resetAdapter(ormContacts);
        ((MainActivity)this.getActivity()).setHeaderText("Whitelist");
        ((MainActivity)this.getActivity()).showInterAd();


    }

    @Override
    public void setTvHintVisibility(ArrayList<OrmContact> ormContacts) {
        super.setTvHintVisibility(ormContacts);
    }

}

