package holo.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.holo.callblocker.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import holo.common.CommonSettings;
import holo.components.MainActivity;
import holo.util.Print;


/**
 * Created by water on 2/19/16.
 */
public class Scheduler extends HoloContactList {

    android.support.v7.widget.SwitchCompat swScheduler;
    ScrollView svScheduler;
    TextView tvTimeTo;
    TextView tvTimeFrom;
    TextView tvTittle;
    Bundle bundle;
    LinearLayout llToolBarBack;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.schedule, container, false);
        swScheduler = ( android.support.v7.widget.SwitchCompat) rootView.findViewById(R.id.swScheduler);
        svScheduler = (ScrollView) rootView.findViewById(R.id.svScheduler);
        tvTimeTo = (TextView)rootView.findViewById(R.id.tvTimeTo);
        tvTimeFrom = (TextView)rootView.findViewById(R.id.tvTimeFrom);

        bundle = this.getArguments();

        if (bundle != null){
            int myInt = bundle.getInt("from_settings", 0);
            if (myInt == 0){
                rootView.findViewById(R.id.container_toolbar_back).setVisibility(View.GONE);
            }else{
                rootView.findViewById(R.id.container_toolbar_back).setVisibility(View.VISIBLE);
                llToolBarBack = (LinearLayout)rootView.findViewById(R.id.toolbarBack);
                llToolBarBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        android.support.v4.app.FragmentManager fm =Scheduler.this.getActivity().getSupportFragmentManager();
                        fm.popBackStack();
                    }
                });
            }
        }else{
            rootView.findViewById(R.id.container_toolbar_back).setVisibility(View.GONE);
        }



        RelativeLayout rlMonday = (RelativeLayout) rootView.findViewById(R.id.rlMonday);

        RelativeLayout rlSchedulerEnable = (RelativeLayout) rootView.findViewById(R.id.rlSchedulerEnable);

        final CheckBox cbMonday = (CheckBox)rootView.findViewById(R.id.cbMonday);
        cbMonday.setChecked(CommonSettings.getInstance().getKEY_SCHEDULER_MONDAY());
        rlMonday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean b = !CommonSettings.getInstance().getKEY_SCHEDULER_MONDAY();
                cbMonday.setChecked(b);
                CommonSettings.getInstance().setKEY_SCHEDULER_MONDAY(b);
            }
        });

        RelativeLayout rlTuesday = (RelativeLayout) rootView.findViewById(R.id.rlTuesday);
        final CheckBox cbTuesday = (CheckBox)rootView.findViewById(R.id.cbTuesday);
        cbTuesday.setChecked(CommonSettings.getInstance().getKEY_SCHEDULER_TUESDAY());
        rlTuesday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean b = !CommonSettings.getInstance().getKEY_SCHEDULER_TUESDAY();
                cbTuesday.setChecked(b);
                CommonSettings.getInstance().setKEY_SCHEDULER_TUESDAY(b);
            }
        });


        RelativeLayout rlWednesday = (RelativeLayout) rootView.findViewById(R.id.rlWednesday);
        final CheckBox cbWednesday= (CheckBox)rootView.findViewById(R.id.cbWednesday);
        cbWednesday.setChecked(CommonSettings.getInstance().getKEY_SCHEDULER_WEDNESDAY());
        rlWednesday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean b = !CommonSettings.getInstance().getKEY_SCHEDULER_WEDNESDAY();
                cbWednesday.setChecked(b);
                CommonSettings.getInstance().setKEY_SCHEDULER_WEDNESDAY(b);
            }
        });


        RelativeLayout rlThursday = (RelativeLayout) rootView.findViewById(R.id.rlThursday);
        final CheckBox cbThursday = (CheckBox)rootView.findViewById(R.id.cbThursday);
        cbThursday.setChecked(CommonSettings.getInstance().getKEY_SCHEDULER_THURSDAY());
        rlThursday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean b = !CommonSettings.getInstance().getKEY_SCHEDULER_THURSDAY();
                cbThursday.setChecked(b);
                CommonSettings.getInstance().setKEY_SCHEDULER_THURSDAY(b);
            }
        });

        RelativeLayout rlFriday = (RelativeLayout) rootView.findViewById(R.id.rlFriday);
        final CheckBox cbFriday = (CheckBox)rootView.findViewById(R.id.cbFriday);
        cbFriday.setChecked(CommonSettings.getInstance().getKEY_SCHEDULER_FRIDAY());
        rlFriday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean b = !CommonSettings.getInstance().getKEY_SCHEDULER_FRIDAY();
                cbFriday.setChecked(b);
                CommonSettings.getInstance().setKEY_SCHEDULER_FRIDAY(b);
            }
        });


        RelativeLayout rlSaturday = (RelativeLayout) rootView.findViewById(R.id.rlSaturday);
        final CheckBox cbSaturday = (CheckBox)rootView.findViewById(R.id.cbSaturday);
        cbSaturday.setChecked(CommonSettings.getInstance().getKEY_SCHEDULER_SATURDAY());
        rlSaturday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean b = !CommonSettings.getInstance().getKEY_SCHEDULER_SATURDAY();
                cbSaturday.setChecked(b);
                CommonSettings.getInstance().setKEY_SCHEDULER_SATURDAY(b);
            }
        });

        RelativeLayout rlSunday = (RelativeLayout) rootView.findViewById(R.id.rlSunday);
        final CheckBox cbSunday = (CheckBox)rootView.findViewById(R.id.cbSunday);
        cbSunday.setChecked(CommonSettings.getInstance().getKEY_SCHEDULER_SUNDAY());
        rlSunday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean b = !CommonSettings.getInstance().getKEY_SCHEDULER_SUNDAY();
                cbSunday.setChecked(b);
                CommonSettings.getInstance().setKEY_SCHEDULER_SUNDAY(b);
            }
        });
        swScheduler.setChecked(CommonSettings.getInstance().getKEY_SCHEDULER_ENABLE());
        disableEnableControls(CommonSettings.getInstance().getKEY_SCHEDULER_ENABLE(), svScheduler);
        // swScheduler.setChecked(CommonSettings.getInstance().getSosLightRandomColorEnabled());
        /*swScheduler.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // CommonSettings.getInstance().setSosLightRandomColorEnabled(isChecked);
                CommonSettings.getInstance().setKEY_SCHEDULER_ENABLE(isChecked);
                disableEnableControls(isChecked, svScheduler);

            }
        });*/

        rlSchedulerEnable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = !swScheduler.isChecked();
                swScheduler.setChecked(isChecked);
                CommonSettings.getInstance().setKEY_SCHEDULER_ENABLE(isChecked);
                disableEnableControls(isChecked, svScheduler);

            }
        });
        rootView.findViewById(R.id.fromTime).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HoloTimePiker timePicker = new HoloTimePiker(Scheduler.this.getActivity(), "From");
                timePicker.show();
            }
        });

        rootView.findViewById(R.id.toTime).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HoloTimePiker timePicker = new HoloTimePiker(Scheduler.this.getActivity(), "To");
                timePicker.show();

            }
        });

        tvTimeTo.setText(CommonSettings.getInstance().getKEY_SCHEDULER_TO());
        tvTimeFrom.setText(CommonSettings.getInstance().getKEY_SCHEDULER_FROM());

        return rootView;
    }

    private void disableEnableControls(boolean enable, ViewGroup vg){
        for (int i = 0; i < vg.getChildCount(); i++){
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup){
                disableEnableControls(enable, (ViewGroup)child);
            }
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        bundle = this.getArguments();

        if (bundle != null){
            int myInt = bundle.getInt("from_settings", 0);
            if (myInt == 0){
                ((MainActivity)this.getActivity()).setHeaderText("Scheduler");

            }else{
                ((TextView)llToolBarBack.findViewById(R.id.tvTitle)).setText("Scheduler");
            }
        }else{
            ((MainActivity)this.getActivity()).setHeaderText("Scheduler");
        }

        ((MainActivity)this.getActivity()).showInterAd();


    }

    public class HoloTimePiker extends Dialog {
        Activity context;
        String type;
        TimePicker timePicker;
        //TimePicker time
        public HoloTimePiker(Activity context, String type) {
            super(context, R.style.PauseDialog);
            this.context = context;
            this.type = type;

        }
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.time_picker_1);

            ((TextView) findViewById(R.id.tvHeader)).setText(type);
            timePicker = (TimePicker)findViewById(R.id.timePicker);
            timePicker.setIs24HourView(false);

            findViewById(R.id.btnDone).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HoloTimePiker.this.dismiss();
                }
            });

            setTimePicker(timePicker, getCustomCal(type), type);


            timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                @Override
                public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {


                    final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                    Date dateObj = null;
                    try {
                        dateObj = sdf.parse(hourOfDay + ":" + minute);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String formatedTime = new SimpleDateFormat("K:mm a").format(dateObj);

                    if (type.equals("From")) {
                        tvTimeFrom.setText(formatedTime);
                        CommonSettings.getInstance().setKEY_SCHEDULER_FROM(formatedTime);
                    } else {
                        tvTimeTo.setText(formatedTime);
                        CommonSettings.getInstance().setKEY_SCHEDULER_TO(formatedTime);
                    }

                }
            });
        }

    }


    /*public class HoloTimePiker extends DialogFragment {

        Activity context;
        String type;
        TimePicker timePicker;
        //TimePicker time
        public HoloTimePiker(Activity context, String type) {
            super();
            this.context = context;
            this.type = type;

        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(Scheduler.this.getActivity());
            AlertDialog mDialog = mBuilder.create();
            mDialog.setView(createDateTimeView(mDialog.getLayoutInflater()));
            //setStyle(R.style.PauseDialog, R.style.AppTheme);
            return mDialog;
        }

        private View createDateTimeView(LayoutInflater layoutInflater) {
            View mView = layoutInflater.inflate(R.layout.time_picker_1, null);
            ((TextView) mView.findViewById(R.id.tvHeader)).setText(type);
            timePicker = (TimePicker)mView.findViewById(R.id.timePicker);
            timePicker.setIs24HourView(false);

            mView.findViewById(R.id.btnDone).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HoloTimePiker.this.dismiss();
                }
            });

            setTimePicker(timePicker, getCustomCal(type), type);


            timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                @Override
                public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {


                    final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                    Date dateObj = null;
                    try {
                        dateObj = sdf.parse(hourOfDay + ":" + minute);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String formatedTime = new SimpleDateFormat("K:mm a").format(dateObj);

                    if (type.equals("From")) {
                        tvTimeFrom.setText(formatedTime);
                        CommonSettings.getInstance().setKEY_SCHEDULER_FROM(formatedTime);
                    } else {
                        tvTimeTo.setText(formatedTime);
                        CommonSettings.getInstance().setKEY_SCHEDULER_TO(formatedTime);
                    }

                }
            });
            return mView;
        }

    }*/


    private void setTimePicker(TimePicker timePicker, Calendar cal, String type){
        if (type.equals("From")){
            cal =  getCustomCal("From");
        }else{
            cal = getCustomCal("To");
        }

        timePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
        Print.e(this, "Minute: "+cal.get(Calendar.MINUTE));
        timePicker.setCurrentMinute(cal.get(Calendar.MINUTE));
    }
    public Calendar getCustomCal(String type){ // for from type-1, from to type-2
        SimpleDateFormat sdf;
        Date date = null;
        try {
            sdf = new SimpleDateFormat("hh:mm a");
            if (type.equals("From")){
                date = sdf.parse(CommonSettings.getInstance().getKEY_SCHEDULER_FROM());
            }else{
                date = sdf.parse(CommonSettings.getInstance().getKEY_SCHEDULER_TO());
            }

        } catch (ParseException e) {
        }

        Calendar c = Calendar.getInstance();
        c.setTime(date);

       /* timePicker.setCurrentHour(c.get(Calendar.HOUR_OF_DAY));
        timePicker.setCurrentMinute(c.get(Calendar.MINUTE));*/
        return  c;

    }
}

