package holo.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.holo.callblocker.R;

import holo.components.MainActivity;

/**
 * Created by water on 4/23/16.
 */
public class Settings extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.settings, container, false);

        rootView.findViewById(R.id.llSchedule).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*android.support.v4.app.FragmentTransaction transaction = Settings.this.getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container_body, new Scheduler());
                transaction.commit();*/

                Scheduler fragment = new Scheduler();
                Bundle bundle = new Bundle();
                bundle.putInt("from_settings", 1);
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container_body2, fragment).addToBackStack(null).
                        commitAllowingStateLoss();
            }
        });

        rootView.findViewById(R.id.llCalls).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container_body2, new BlockCalls()).addToBackStack(null).
                        commitAllowingStateLoss();
            }
        });

        rootView.findViewById(R.id.llBlockAndSendSms).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container_body2, new SendSMS()).addToBackStack(null).
                        commitAllowingStateLoss();
            }
        });


        rootView.findViewById(R.id.llPasswordProtection).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container_body2, new PasswordProtection()).addToBackStack(null).
                        commitAllowingStateLoss();
            }
        });

        rootView.findViewById(R.id.llNotification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container_body2, new BlockNotification()).addToBackStack(null).
                        commitAllowingStateLoss();
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)this.getActivity()).setHeaderText("Settings");
        ((MainActivity)this.getActivity()).showInterAd();


    }

}

