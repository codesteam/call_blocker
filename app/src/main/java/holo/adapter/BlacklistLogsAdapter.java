package holo.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.holo.callblocker.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import holo.components.MainActivity;
import holo.db.DBHelper;
import holo.fragments.BlacklistLogs;
import holo.model.BlockCallLog;

/**
 * Created by water on 2/5/16.
 */
public class BlacklistLogsAdapter extends BaseAdapter  {
    private MainActivity context;
    BlacklistLogs blacklist;
    private ArrayList<BlockCallLog>originalData = null;
    private LayoutInflater mLayoutInflater;
    DBHelper mydb;

    public BlacklistLogsAdapter(BlacklistLogs context, ArrayList<BlockCallLog> blacklistContacts) {
        this.context = (MainActivity)context.getActivity();
        this.blacklist = context;
        this.originalData = blacklistContacts;
        mLayoutInflater = (LayoutInflater) this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mydb = new DBHelper(this.context);

    }


    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.item_black_list_log, null);
        }

        TextView textViewTitle = (TextView) view.findViewById(R.id.tvName);
        TextView textViewTime = (TextView) view.findViewById(R.id.tvTimeText);
        TextView tvNumber = (TextView) view.findViewById(R.id.tvNumber);
        BlockCallLog blockCallLog = originalData.get(position);
        Date date = new Date(blockCallLog.getTime());
        DateFormat formatter = new SimpleDateFormat("dd-MMM, hh:mm a", Locale.ENGLISH);
        String dateFormatted = formatter.format(date);
        textViewTime.setText(dateFormatted+"");
        String name = originalData.get(position).getName();
        textViewTitle.setText(name + "");
        tvNumber.setText(originalData.get(position).getNumber());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DeleteOrCall(originalData.get(position)).show();
            }
        });
        return view;
    }

    @Override
    public int getCount() {
        return originalData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void resetAdapter(ArrayList<BlockCallLog> blockCallLogs){
        this.originalData = blockCallLogs;
        this.notifyDataSetChanged();

    }

    public class DeleteOrCall extends Dialog {

       BlockCallLog blockCallLog;
        public DeleteOrCall(BlockCallLog blockCallLog) {
            super(context, R.style.PauseDialog);
            this.blockCallLog = blockCallLog;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.delete_or_call_item);

            ((TextView)findViewById(R.id.item)).setText(blockCallLog.getName().toString() + "");
            findViewById(R.id.btnDelete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mydb.deleteBlockCallLog(blockCallLog.getId());
                    BlacklistLogsAdapter.this.resetAdapter(mydb.getBlockLogs());
                    DeleteOrCall.this.dismiss();

                }
            });

            findViewById(R.id.btnCall).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + blockCallLog.getNumber()));
                    context.startActivity(callIntent);
                    DeleteOrCall.this.dismiss();
                }
            });
        }

    }
}