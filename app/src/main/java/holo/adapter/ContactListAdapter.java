package holo.adapter;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.TextView;

import com.holo.callblocker.R;

import java.util.ArrayList;
import java.util.List;

import holo.db.DBHelper;
import holo.dialog.CategoryChanger;
import holo.model.CatChangerListner;
import holo.model.OrmContact;

/**
 * Created by water on 2/5/16.
 */
public class ContactListAdapter extends BaseAdapter {
    private Activity context;
    private List<OrmContact>originalData = null;
    private List<OrmContact>filteredData = null;
    private LayoutInflater mInflater;
    private ItemFilter mFilter = new ItemFilter();
    private LayoutInflater mLayoutInflater;
    DBHelper mydb;
    FloatingActionButton fabDone;

    int cat; // 0 for balck list, 1 for white list
    public ContactListAdapter(Context context,  ArrayList<OrmContact> ormContacts, int cat, FloatingActionButton fabDone) {
        this.context = (Activity)context;
        this.originalData = ormContacts;
        this.filteredData = ormContacts;
        mLayoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mydb = new DBHelper(context);
        this.cat = cat;
        this.fabDone = fabDone;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.item_contact, null);
        }
        TextView textViewTitle = (TextView) view.findViewById(R.id.tvName);
        TextView tvImage = (TextView) view.findViewById(R.id.ivImage);
        final CheckBox checkBox = (CheckBox)view.findViewById(R.id.cbItemSelected);

        /*view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contacts.get(position).setSelected(!contacts.get(position).isSelected());
                checkBox.setChecked(contacts.get(position).isSelected());
            }
        });*/

            /*checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    filteredData.get(position).setSelected(isChecked);
                    //Print.e(this, position);
                    if (isChecked) {
                        Contact c = filteredData.get(position);
                        if (c.getType() == 1) { // call logs
                            for (int i = 0; i < c.getNumbers().size(); i++) {
                                mydb.insertHoloContact(c.getName(), c.getNumbers().get(i), c.getContactId(), System.currentTimeMillis());
                            }
                        } else if (c.getType() == 2) { // contacts
                            Cursor phones = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + c.getContactId(), null, null);
                            while (phones.moveToNext()) {
                                String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                // int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                                Print.e(this, number);
                                mydb.insertHoloContact(c.getName(), number, c.getContactId(), System.currentTimeMillis());
                                Print.e(this, "Insert complete");

                               *//* switch (type) {
                                    case Phone.TYPE_HOME:
                                        // do something with the Home number here...
                                        break;
                                    case Phone.TYPE_MOBILE:
                                        // do something with the Mobile number here...
                                        break;
                                    case Phone.TYPE_WORK:
                                        // do something with the Work number here...
                                        break;
                                }*//*
                            }
                            phones.close();
                        }

                        ToastMsg.Toast(context, "Added "+ c.getName() +" into " + from);
                    } else {
                        mydb.deleteHoloContactByName(filteredData.get(position).getName());
                        ToastMsg.Toast(context, "Removed " +  filteredData.get(position).getName() + " from " + from);

                    }

                }
            });*/
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = !checkBox.isChecked();
                filteredData.get(position).setSelected(isChecked);
                checkBox.setChecked(isChecked);
                //Print.e(this, position);

                if (isChecked) {

                    OrmContact c = filteredData.get(position);
                    final ArrayList<OrmContact> ormContacts = mydb.getHoloContactListByName(c.getName());
                    if (ormContacts.size() > 0) { // since already this contact added just update the category
                        new CategoryChanger(context, new CatChangerListner() {
                            @Override
                            public void categoryChanged() {
                                for (int j = 0; j < ormContacts.size(); j++) {
                                    OrmContact hc = ormContacts.get(j);
                                    mydb.updateHoloContact((hc.getDatabaseId()), hc.getName(), hc.getNumbers().get(0), hc.getRefId(),
                                            System.currentTimeMillis(), cat);
                                }
                            }

                            @Override
                            public void categoryNotChanged() {
                                checkBox.setChecked(false);
                            }
                        }, cat, c.getName()).show();
                    } else { // insert new holo contact
                        if (c.getType() == 1) { // from call logs
                            for (int i = 0; i < c.getNumbers().size(); i++) {
                                mydb.insertHoloContact(c.getName(), c.getNumbers().get(i), c.getRefId(), System.currentTimeMillis(), cat);
                            }
                        } else if (c.getType() == 2) { // from contacts list
                            Cursor phones = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + c.getRefId(), null, null);
                            while (phones.moveToNext()) {
                                String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                mydb.insertHoloContact(c.getName(), number, c.getRefId(), System.currentTimeMillis(), cat);

                            }
                            phones.close();
                        }

                        //ToastMsg.Toast(context, "Added "+ c.getName() +" into " + from);
                    }

                    fabDone.setVisibility(View.VISIBLE);

                } else {
                    mydb.deleteHoloContactByName(filteredData.get(position).getName());
                    //ToastMsg.Toast(context, "Removed " +  filteredData.get(position).getName() + " from " + from);

                }
            }
        });

       /* checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = ((CheckBox) v).isChecked();

            }
        });*/
        checkBox.setChecked(filteredData.get(position).isSelected());
        String name = filteredData.get(position).getName();
        textViewTitle.setText(name + "");
        if (name != null && name.length() > 0){
            tvImage.setText(name.toUpperCase().charAt(0)+"");
        }

        return view;
    }

    /* public class CategoryChanger extends Dialog {
         ArrayList<OrmContact> ormContacts;
         CheckBox checkBox;
         public CategoryChanger( ArrayList<OrmContact> ormContacts, CheckBox checkBox) {
             super(context, R.style.PauseDialog);
             this.ormContacts = ormContacts;
             this.checkBox = checkBox;
         }

         @Override
         protected void onCreate(Bundle savedInstanceState) {
             super.onCreate(savedInstanceState);
             setContentView(R.layout.category_changer);

             findViewById(R.id.btnYes).setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     for (int j = 0; j < ormContacts.size(); j++) {
                         OrmContact hc = ormContacts.get(j);
                         mydb.updateHoloContact((hc.getDatabaseId()), hc.getName(), hc.getNumbers().get(0), hc.getRefId(),
                                 System.currentTimeMillis(), cat);
                         //ToastMsg.Toast(context, "Added "+ c.getName() +" into " + from);
                     }
                     CategoryChanger.this.dismiss();

                 }
             });

             findViewById(R.id.btnNo).setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     CategoryChanger.this.dismiss();
                     checkBox.setChecked(false);

                 }
             });

             String header = "";
             String message = "";

             if (cat == 0){
                 header = "Move to Black list";
                 message = "This number is listed in white list do you want to move it to blacklist?";
             }else{
                 header = "Move to White list";
                 message = "This number is listed in black list do you want to move it to whitelist?";
             }
             ((TextView) findViewById(R.id.tvHeader)).setText(header);
             ((TextView) findViewById(R.id.tvMessage)).setText(message);
         }

     }*/
    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<OrmContact> list = originalData;

            int count = list.size();
            final ArrayList<OrmContact> nlist = new ArrayList<OrmContact>(count);

            String filterableString ;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getName();
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(list.get(i));
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<OrmContact>) results.values;
            notifyDataSetChanged();
        }

    }
}