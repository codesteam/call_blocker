package holo.adapter;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.holo.callblocker.R;

import java.util.ArrayList;

import holo.components.MainActivity;
import holo.datetime.DateTimeConverter;
import holo.db.DBHelper;
import holo.dialog.SettingBlacklistItem;
import holo.fragments.HoloContactList;
import holo.model.OrmContact;

/**
 * Created by water on 2/5/16.
 */
public class BlacklistAdapter extends  RecyclerView.Adapter<BlacklistAdapter.MyViewHolder>  {
    private MainActivity context;
    HoloContactList blacklist;
    private ArrayList<OrmContact>originalData = null;
    private LayoutInflater mLayoutInflater;
    DBHelper mydb;
    int cat;
    LayoutInflater inflater;
    public BlacklistAdapter(HoloContactList context, ArrayList<OrmContact> ormContacts, int cat) {
        this.context = (MainActivity)context.getActivity();
        this.blacklist = context;
        this.originalData = ormContacts;
        mLayoutInflater = (LayoutInflater) this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mydb = new DBHelper(this.context);
        this.cat = cat;
        inflater = LayoutInflater.from(this.context);

    }

    private void setTvBlockTime(TextView tvBlockTime, OrmContact ormContact){

        tvBlockTime.setTextColor(context.getResources().getColor(android.R.color.secondary_text_dark));
        if(ormContact.getBlockUntilTime()> 0 && ormContact.getBlockUntile() > 0){
            if (ormContact.getBlockUntilTime() > System.currentTimeMillis()){
                tvBlockTime.setText("Block Until: "+ DateTimeConverter.getFormatedDate(ormContact.getBlockUntilTime(), "dd/MM/yyyy hh:mm: aa"));
                tvBlockTime.setTextColor(context.getResources().getColor(R.color.Green));
            }else{
                tvBlockTime.setText("Time out: "+DateTimeConverter.getFormatedDate(ormContact.getBlockUntilTime(), "dd/MM/yyyy hh:mm: aa"));
                tvBlockTime.setTextColor(context.getResources().getColor(R.color.Warnning));
            }
        }else{
            tvBlockTime.setText(context.getResources().getString(R.string.set_timer_for_block_until));
        }
    }
    public void resetAdapter(ArrayList<OrmContact> ormContacts){
        this.originalData = ormContacts;
        this.notifyDataSetChanged();

    }
    public void resetAdapter(){
        this.originalData = mydb.getHoloContactList(cat);
        this.notifyDataSetChanged();
        blacklist.setTvHintVisibility(originalData);

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_black_list, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final OrmContact ormContact = originalData.get(position);
        holder.tvCat.setText(ormContact.getName().toUpperCase().charAt(0) + "");


        if (ormContact.getSendMessage() == 1 && ormContact.getMessage() != null){
            holder.ivSmsStatus.setImageResource(R.drawable.ic_sms_active);
        }else{
            holder.ivSmsStatus.setImageResource(R.drawable.ic_sms_inactive);
        }

        if (ormContact.getBlockUntile() == 1 && ormContact.getBlockUntilTime()>0 && ormContact.getBlockUntilTime() > System.currentTimeMillis()) {
            holder.ivBlockUntilStatus.setImageResource(R.drawable.ic_cal_active);
        }else if (ormContact.getBlockUntile() == 1 && ormContact.getBlockUntilTime()>0 && ormContact.getBlockUntilTime() < System.currentTimeMillis()) {
            holder.ivBlockUntilStatus.setImageResource(R.drawable.ic_cal_out_of_time);
        }else{
            holder.ivBlockUntilStatus.setImageResource(R.drawable.ic_cal_inactive);
        }
        holder.ivSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SettingBlacklistItem(context, ormContact, BlacklistAdapter.this).show();
            }
        });
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DeleteItem(ormContact).show();
            }
        });

        String name = originalData.get(position).getName();
        holder.textViewTitle.setText(name + "");

        setTvBlockTime(holder.textViewWarrning, ormContact);

        if (ormContact.getAfterTimeOut() == 1){
            mydb.deleteHoloContact(ormContact.getDatabaseId());

        }

       // handler.post(r);
    }



    @Override
    public void onViewRecycled(MyViewHolder holder) {
        super.onViewRecycled(holder);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
         return originalData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textViewTitle;
        TextView textViewWarrning;
        TextView tvCat;
        ImageView ivSettings;
        ImageView ivSmsStatus;
        ImageView ivBlockUntilStatus;
        View item;
        public MyViewHolder(View view) {
            super(view);
             textViewTitle = (TextView) view.findViewById(R.id.tvName);
             textViewWarrning = (TextView) view.findViewById(R.id.tvWarnning);
             ivSettings = (ImageView) view.findViewById(R.id.ivSettings);

             ivSmsStatus = (ImageView) view.findViewById(R.id.ivSmsStatus);
             ivBlockUntilStatus = (ImageView) view.findViewById(R.id.ivBlockUntilStatus);
            tvCat = (TextView) view.findViewById(R.id.tvCat);
            item = view;
        }
    }

    public class DeleteItem extends Dialog {


        OrmContact ormContact;
        public DeleteItem( OrmContact ormContact) {
            super(context, R.style.PauseDialog);
            this.ormContact = ormContact;

        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.delete_item);

            ((TextView)findViewById(R.id.item)).setText(ormContact.getName().toString()+"");
            findViewById(R.id.btnDelete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mydb.deleteHoloContactByName(ormContact.getName());
                    resetAdapter();
                    DeleteItem.this.dismiss();

                }
            });
        }

    }
}