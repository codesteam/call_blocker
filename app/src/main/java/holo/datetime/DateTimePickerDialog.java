package holo.datetime;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.holo.callblocker.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import holo.util.Print;


public class DateTimePickerDialog extends DialogFragment {
	public static final String TAG_FRAG_DATE_TIME = "fragDateTime";
	private static final String KEY_DIALOG_TITLE = "dialogTitle";
	private static final String KEY_INIT_DATE = "initDate";
	private static final String TAG_DATE = "date";
	private static final String TAG_TIME = "time";
	private Context mContext;
	private ButtonClickListener mButtonClickListener;
	private OnDateTimeSetListener mOnDateTimeSetListener;
	private Bundle mArgument;
	private DatePicker mDatePicker;
	private TimePicker mTimePicker;
	private int type = -1;
	private int position = -1;
	
	public boolean datePickerEnable = false;
	public boolean timePickerEnable = false;
	Calendar calendar;
	// DialogFragment constructor must be empty
	public DateTimePickerDialog() {
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mContext = activity;
		mButtonClickListener = new ButtonClickListener();
	}

	/**
	 * 
	 * @param dialogTitle
	 *            Title of the DateTimePicker DialogFragment
	 * @param initDate
	 *            Initial Date and Time set to the Date and Time Picker
	 * @return Instance of the DateTimePicker DialogFragment
	 */
	public static DateTimePickerDialog newInstance(CharSequence dialogTitle,
			Date initDate, boolean datePickerEnable,  boolean timePickerEnable) {
	
		// Create a new instance of DateTimePicker
		DateTimePickerDialog mDateTimePicker = new DateTimePickerDialog();
		mDateTimePicker.datePickerEnable = datePickerEnable;
		mDateTimePicker.timePickerEnable = timePickerEnable;
		// Setup the constructor parameters as arguments
		Bundle mBundle = new Bundle();
		mBundle.putCharSequence(KEY_DIALOG_TITLE, dialogTitle);
		mBundle.putSerializable(KEY_INIT_DATE, initDate);
		mDateTimePicker.setArguments(mBundle);
		// Return instance with arguments
		return mDateTimePicker;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Retrieve Argument passed to the constructor
		mArgument = getArguments();
		// Use an AlertDialog Builder to initially create the Dialog
		AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
		// Setup the Dialog
		calendar = Calendar.getInstance();
		String title = "";

		if (datePickerEnable) {
			title = "Set Date";
		}

		if (timePickerEnable) {
			title = "Set Time";
		}

		if (datePickerEnable && timePickerEnable) {
			title = "Set Date & Time";
		}
		TextView tvTitle = new TextView(mContext);
		tvTitle.setText(title);
		tvTitle.setBackgroundColor(mContext.getResources().getColor(R.color.Silver));
		tvTitle.setPadding(10, 10, 10, 10);
		tvTitle.setGravity(Gravity.CENTER);
		tvTitle.setTextColor(mContext.getResources().getColor(R.color.Black));
		tvTitle.setTextSize(20);
		mBuilder.setCustomTitle(tvTitle);
		mBuilder.setNegativeButton(android.R.string.no, mButtonClickListener);
		mBuilder.setPositiveButton(android.R.string.yes, mButtonClickListener);
		// Create the Alert Dialog
		AlertDialog mDialog = mBuilder.create();
		// Set the View to the Dialog
		mDialog.setView(createDateTimeView(mDialog.getLayoutInflater()));
		// Return the Dialog created
		return mDialog;
	}

	/**
	 * Inflates the XML Layout and setups the tabs
	 * 
	 * @param layoutInflater
	 *            Layout inflater from the Dialog
	 * @return Returns a view that will be set to the Dialog
	 */
	private View createDateTimeView(LayoutInflater layoutInflater) {
		// Inflate the XML Layout using the inflater from the created Dialog
		View mView = layoutInflater.inflate(R.layout.date_time_picker, null);
		// Extract the TabHost
	/*	TabHost mTabHost = (TabHost) mView.findViewById(R.id.tab_host);
		mTabHost.setup();
		// Create Date Tab and add to TabHost
		TabHost.TabSpec mDateTab = mTabHost.newTabSpec(TAG_DATE);
		mDateTab.setIndicator("Date");
		mDateTab.setContent(R.id.date_content);
		mTabHost.addTab(mDateTab);
		// Create Time Tab and add to TabHost
		TabHost.TabSpec mTimeTab = mTabHost.newTabSpec(TAG_TIME);
		mTimeTab.setIndicator("Time");
		mTimeTab.setContent(R.id.time_content);
		mTabHost.addTab(mTimeTab);*/
		// Retrieve Date from Arguments sent to the Dialog
		final DateTime mDateTime = new DateTime(
				(Date) mArgument.getSerializable(KEY_INIT_DATE));
		// Initialize Date and Time Pickers
		mDatePicker = (DatePicker) mView.findViewById(R.id.date_picker);
		mDatePicker.setMinDate(System.currentTimeMillis() - 1000);
		LinearLayout llDatePicker = (LinearLayout) mView.findViewById(R.id.date_content);
		LinearLayout llTimePicker = (LinearLayout) mView.findViewById(R.id.time_content);


		if (datePickerEnable)
		{
			llDatePicker.setVisibility(View.VISIBLE);
			mDatePicker.init(mDateTime.getYear(), mDateTime.getMonthOfYear(),
					mDateTime.getDayOfMonth(), null);



		}
		else
		{
			llDatePicker.setVisibility(View.GONE);
		}
	
		mTimePicker = (TimePicker) mView.findViewById(R.id.time_picker);

		final String today = mDateTime.getYear()+""+mDateTime.getMonthOfYear()+ "" +mDateTime.getDayOfMonth();


		//mTimePicker.set
		if (timePickerEnable)
		{
			llTimePicker.setVisibility(View.VISIBLE);
			mTimePicker.setCurrentHour(mDateTime.getHourOfDay());
			mTimePicker.setCurrentMinute(mDateTime.getMinuteOfHour());
		}
		else
		{
			llTimePicker.setVisibility(View.GONE);
		}

		mTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
			@Override
			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
				String changedDate = mDatePicker.getYear() + "" + ( mDatePicker.getMonth()) + ""+ mDateTime.getDayOfMonth();

				if (changedDate!= today){
					//mTimePicker.setCurrentHour(mDateTime.getHourOfDay());
					//mTimePicker.setCurrentMinute(mDateTime.getMinuteOfHour());
					Print.e(this, "if");
				}else{
					Print.e(this, "else");
				}
			}
		});
		
		// Return created view
		return mView;
	}

	private Date getPickerDate(DateTime mDateTime){
		Date todayDate = null;
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String sday = (""+mDateTime.getDayOfMonth());
		String smonth=("-"+mDateTime.getMonthOfYear());
		String syear=("-"+mDateTime.getYear());
		String y=(""+sday+smonth+syear);
		try
		{
			todayDate = formatter.parse(y);

		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return  todayDate;
	}
	/**
	 * Sets the OnDateTimeSetListener interface
	 * 
	 * @param onDateTimeSetListener
	 *            Interface that is used to send the Date and Time to the
	 *            calling object
	 */
	public void setOnDateTimeSetListener(
			OnDateTimeSetListener onDateTimeSetListener) {
		mOnDateTimeSetListener = onDateTimeSetListener;
	}

	public void setOnDateTimeSetListener(
			OnDateTimeSetListener onDateTimeSetListener, int type, int position) {
		mOnDateTimeSetListener = onDateTimeSetListener;
		this.type = type;
		this.position = position;
	}

	private class ButtonClickListener implements
			DialogInterface.OnClickListener {
		@Override
		public void onClick(DialogInterface dialogInterface, int result) {
			// Determine if the user selected Ok
			if (DialogInterface.BUTTON_POSITIVE == result) {
				DateTime mDateTime = new DateTime(mDatePicker.getYear(),
						mDatePicker.getMonth(), mDatePicker.getDayOfMonth(),
						mTimePicker.getCurrentHour(),
						mTimePicker.getCurrentMinute());

				if (type == -1) {
					mOnDateTimeSetListener.DateTimeSet(mDateTime.getDate());

				} else {
					mOnDateTimeSetListener.DateTimeSet(mDateTime.getDate(),
							type, position);

				}
			}
			if (DialogInterface.BUTTON_NEGATIVE == result){
				mOnDateTimeSetListener.ClearDate();
			}
		}
	}

	/**
	 * Interface for sending the Date and Time to the calling object
	 */
	public interface OnDateTimeSetListener {
		public void DateTimeSet(Date date);
		void ClearDate();
		public void DateTimeSet(Date date, int type, int position); // type = 0
																	// start
																	// date,
																	// type = 1
																	// end date,
																	// position
																	// =
																	// position
																	// of
																	// element
																	// in array
																	// holoContactlistAdapter
	}
}
