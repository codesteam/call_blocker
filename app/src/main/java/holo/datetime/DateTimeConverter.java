package holo.datetime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeConverter {

	public static long hrPassedOfThisDayInMillis()
	{
		Calendar c = Calendar.getInstance();
	    c.setTimeInMillis(System.currentTimeMillis());

		return (long)((c.get(Calendar.HOUR_OF_DAY) * 3600 * 1000)) ;
	}
	
	public static long minPassedOnThisHrInMills()
	{
		Calendar c = Calendar.getInstance();
	    c.setTimeInMillis(System.currentTimeMillis());

		return (long)((c.get(Calendar.MINUTE) * 60 * 1000)) ;
	}
	
	public static long dayPassedOfThisWeekInMillis()
	{
		Calendar c = Calendar.getInstance();
	    c.setTimeInMillis(System.currentTimeMillis());
	    //Print.e("date time", c.get(Calendar.DAY_OF_WEEK));
	    
	    return DateTimeConverter.getInMillis(c.get(Calendar.DAY_OF_WEEK)) ;
	}
	
	public static long dayPassedOfThisMonthInMillis()
	{
		Calendar c = Calendar.getInstance();
	    c.setTimeInMillis(System.currentTimeMillis());
	    
	   // Print.e("date time", c.get(Calendar.DAY_OF_MONTH));
	    
	    return DateTimeConverter.getInMillis(c.get(Calendar.DAY_OF_MONTH)) ;
	}
	
	public static long getInMillis(int total_day)
	{
		return (long)total_day * 24 * 3600 * 1000;
	}
	
	public static String getFormatedDate(long milli_seconds, String date_format) // date_format = "dd/MM/yyyy hh:mm:ss.SSS"
	{
	     SimpleDateFormat formatter = new SimpleDateFormat(date_format);

	     Calendar calendar = Calendar.getInstance();
	     calendar.setTimeInMillis(milli_seconds);
	     
	     return formatter.format(calendar.getTime());
	}
	
	public static Long convertDateToMillis(String date_str, String formate)
	{
		String toParse = date_str; 
		SimpleDateFormat formatter = new SimpleDateFormat(formate); 
		Date date = null;
		Long millis = null;
		
		try {
			date = formatter.parse(toParse);
			millis = date.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return millis;
	}
	
	public static String convertOneToAnotherFormate(String date_str, String from, String to) // date_str ="2015/05/08" from = "yyyy/MM/dd" to = "dd/MM/yyyy"
	{
		String toParse = date_str; 
		SimpleDateFormat formatter = new SimpleDateFormat(from); 
		SimpleDateFormat formatter1 = new SimpleDateFormat(to);
		Date date = null;
		Long millis = null;
		String formated_date = null;
		try {
			date = formatter.parse(toParse);
			millis = date.getTime();
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(millis);
			formated_date = formatter1.format(calendar.getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return formated_date;
	}
	public static long convertDateToMillis(Date date)
	{
		return date.getTime();
	}
	
	public static long getCurrentTimeInUTC()
	{
		return System.currentTimeMillis() - 6 *3600*1000;
	}
	
	public static long convertUtcTimeToLocalTime(long utcTime)
	{
		return utcTime + 6 *3600*1000;

	}
	
	public static long timeCovertToUTC(long time)
	{
		return time - 6 *3600*1000;
	}
}
