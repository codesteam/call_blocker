package materialdesign.utility;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.holo.callblocker.R;


abstract public class FragmentParent extends Fragment {

    private final int ILLEGAL_ID = Integer.MIN_VALUE;

    private int fragmentHolderId = ILLEGAL_ID;

    /**
     * Called by default when switching in-between sliding menu item.
     * Suppose you are switching from Targets(menu item) to Task(menu item).
     * Targets hide() function will be called by default.
     * To stop this default behaviour override this method.
     */
    public void  hide(){

        String name = getStackTopName();

        if (name != null){
            ((FragmentNested)getChildFragmentManager().findFragmentByTag(name)).hide();
        }
    }

    /**
     * Called by default when switching in-between sliding menu item.
     * Suppose you are switching from Targets(menu item) to Task(menu item).
     * Tasks awake() function will be called by default.
     * To stop this default behaviour override this method.
     */
    public void awake() {

        String name = getStackTopName();

        if (name != null){
            ((FragmentNested)getChildFragmentManager().findFragmentByTag(name)).awake();
        }

    }
    private void onBackPressed()
    {
        String name = getStackTopName();

        if (name != null){
            ((FragmentNested)getChildFragmentManager().findFragmentByTag(name)).onBackPressed();
        }
    }
    private String getStackTopName(){
        FragmentManager.BackStackEntry backEntry = null;
        try {
            backEntry = getChildFragmentManager().getBackStackEntryAt(getChildFragmentManager().getBackStackEntryCount() - 1);
            return backEntry.getName();
        }
        catch (Exception ex) {
            Log.e("FragmentParent", "BackStackEntryCount 0");
            ex.printStackTrace();
            return  null;
        }


    }

    private Fragment getTop() {

        String name = getStackTopName();

        if (name != null){
            return getChildFragmentManager().findFragmentByTag(name);
        }
        else{
            return  null;
        }
    }

    /**
     * For initializing a layout as container of this fragment.
     * @param layoutId
     */
    public void initialize(int layoutId) {

        if (layoutId == ILLEGAL_ID) {

            throw new IllegalArgumentException("Invalid View Layout ID");
        }

        this.fragmentHolderId = layoutId;
    }

    private void reloadNestedFragment(){
        String name = getStackTopName();

        if (name != null){
            ((FragmentNested)getChildFragmentManager().findFragmentByTag(name)).reload();
        }
    }

    /**
     * For reloading parent backStack Top.
     * This will call onCreateView of child that are currently in backStack top.
     */
    public void reload() {

        reloadNestedFragment();

        Fragment frag = getTop();

        if (frag != null){
            final FragmentTransaction ft = getChildFragmentManager().beginTransaction();
            ft.detach(frag);
            ft.attach(frag);
            ft.commitAllowingStateLoss();
        }
        else{
            Log.e("FragmentParent", "Reload failed: BackStackEntryCount 0");
        }

    }

    /**
     * For adding a new fragment into backStack
     * @param destination
     */
    public void addFragment(FragmentNested destination) {

        if (this.fragmentHolderId == ILLEGAL_ID) {
            throw new IllegalStateException(
                    "Did not set view layout for fragment transition.");
        }

        destination.setParent(this);

        FragmentManager transaction = getChildFragmentManager();
        String className = destination.getClass().getName();

		/*getChildFragmentManager().addOnBackStackChangedListener(new OnBackStackChangedListener() {

			@Override
			public void onBackStackChanged() {
				// TODO Auto-generated method stub
				awake();
			}
		});*/

        Log.e(this.getClass().getSimpleName(),
                "Count:66y " + transaction.getBackStackEntryCount());

        if (transaction.getBackStackEntryCount() > 0) {

            transaction
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_right,
                            R.anim.slide_out_left,
                            R.anim.slide_in_left,
                            R.anim.slide_out_right)
                    .replace(this.fragmentHolderId, destination, className)
                    .addToBackStack(className).commit();
        } else {

            transaction.beginTransaction()
                    .add(this.fragmentHolderId, destination, className)
                    .addToBackStack(className).commit();
        }

    }

    /**
     * Called from main activity when hardware back pressed.
     * @return
     */
    public boolean onBackEvent() {

        onBackPressed();
        return  popBack(1);
    }

    /**
     * Called from nested fragment
     * @param destination
     */
    public void backTo(Class destination) {

        FragmentManager transaction = getChildFragmentManager();

        Log.i(this.getClass().getSimpleName(),
                "Count " + transaction.getBackStackEntryCount());

        transaction.popBackStack(destination.getName(), 0);
    }

    /**
     * Called from nested fragment
     */
    public void goBack() {
        popBack(1);

    }

    private boolean popBack(int bound){
        FragmentManager transaction = getChildFragmentManager();
        Log.e(this.getClass().getSimpleName(),
                "Count " + transaction.getBackStackEntryCount());


        if (transaction.getBackStackEntryCount() > bound) {
            transaction.popBackStack();
            return false;
        } else {

            return true;
        }
    }

    /**
     * Called from Nested Fragment
     * @param with
     */
    public void replace(FragmentNested with){
        popBack(0);
        addFragment(with);
    }

    /**
     * Called from Nested Fragment
     * @param target
     * @param with
     */
    public void replace(Class target, FragmentNested with){
        backTo(target);
        addFragment(with);
    }

    public void setTitle(String title){
    }
}
