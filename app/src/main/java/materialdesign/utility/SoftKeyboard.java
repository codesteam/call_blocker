package materialdesign.utility;

import android.app.Activity;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class SoftKeyboard {
	public static void HideSoftKeyboard(FragmentActivity context) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Activity.INPUT_METHOD_SERVICE);

		View currentFocus = context.getCurrentFocus();
		IBinder token = currentFocus == null ? null : currentFocus
				.getWindowToken();
		if (token != null)
			imm.hideSoftInputFromWindow(token, 0);
	}
}
