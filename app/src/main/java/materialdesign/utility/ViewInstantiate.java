package materialdesign.utility;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.ViewGroup;


/**
 * Created by water on 6/8/15.
 */
public class ViewInstantiate {

    public static void Instantiate(Activity context) {


        ViewGroup main_view = (ViewGroup) context.getWindow().getDecorView()
                .findViewById(android.R.id.content);

        Typeface typeFace = Typeface.createFromAsset(context.getAssets(),"fonts/MYRIADPRO-REGULAR.OTF");

        TypefaceHelper.setViewGroupTypeface(main_view, typeFace);

    }
}
