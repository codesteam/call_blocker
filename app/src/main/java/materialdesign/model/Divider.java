package materialdesign.model;

import android.graphics.Color;

/**
 * Created by water on 5/17/15.
 */
public class Divider {
    public static final int DIVIDER_TOP = 0;
    public static final int DIVIDER_BELLOW = 1;

    int dividerColor = Color.parseColor("#A0A0A0");
    int height = 1;
    int textColor = Color.parseColor("#22B573");;
    String text = "Header";
    int textSize = 20;
    int gravity = DIVIDER_TOP; // 0 - item top, 1 - item bellow

    public Divider(){

    }
    public Divider(String text, int textColor, int textSize, int dividerColor, int height, int gravity){
        this.dividerColor = dividerColor;
        this.height = height;
        this.text = text;
        this.textColor = textColor;
        this.gravity = gravity;
        this.textSize = textSize;
    }
    public int getDividerColor(){
        return  dividerColor;
    }

    public int getHeight(){
        return height;
    }
    public String getText(){
        return text;
    }
    public int getGravity(){
        return  gravity;
    }
    public int getTextColor(){
        return  textColor;
    }
    public int getTextSize()
    {
        return textSize;
    }
    public void setDividerColor(int dividerColor){
        this.dividerColor = dividerColor;
    }

    /**
     * if text = null only then height will be counted for drawing line divider.
     * @param height
     */
    public  void setHeight(int height){
        this.height = height;
    }
    public void setTextColor(int textColor){
        this.textColor = textColor;
    }

    /**
     * if text = null then line divider will enable
     * @param text
     */
    public void setText(String text){
        this.text = text;
    }
    public void setTextSize(int textSize){
        this.textSize = textSize;
    }

    /**
     *  Divider.DIVIDER_TOP for drawing divider on item top.
     *  Divider.DIVIDER_BELLOW for drawing divider bellow item.
     * @param gravity
     */
    public void setGravity(int gravity){
        this.gravity = gravity;
    }
}
