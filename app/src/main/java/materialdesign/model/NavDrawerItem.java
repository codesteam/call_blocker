package materialdesign.model;


import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;


public  class NavDrawerItem  {
    protected boolean showNotify;
    private String title;
    String subTitle;
    protected int selectorImageView;
    protected int textSize;
    protected int textColor;
    protected int view_id;
    protected FragmentManager fragmentManager;
    protected ActionBarActivity context;
    protected Divider[] params;
    public NavDrawerItem() {

    }

    public NavDrawerItem(boolean showNotify, String title) {
        this.showNotify = showNotify;
        this.title = title;
    }
    public NavDrawerItem(ActionBarActivity context, boolean showNotify, String title, String subTitle, int selectorImageView, int textSize,int textColor, Divider... params)
    {
        this.showNotify = showNotify;
        this.title = title;
        this.subTitle = subTitle;
        this.selectorImageView = selectorImageView;
        this.textSize = textSize;
        this.textColor = textColor;
        this.context = context;
        this.params = params;
        fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();

    }

    public boolean isShowNotify() {
        return showNotify;
    }

    public void setShowNotify(boolean showNotify) {
        this.showNotify = showNotify;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        context.setTitle(title + "");
    }

    /**
     *To intercept sliding menu click event. Call super.fire() to enable default behaviour.
     */
    public  void fire(){
    }

    public int getSelectorImageView()
    {
        return selectorImageView;
    }

    public int getTextSize()
    {
        return textSize;
    }

    public int getMenuTextColor()
    {
        return textColor;
    }

    public void setMenuTextColor(int textColor)
    {
        this.textColor = textColor;
    }

    public Divider[] getDivider(){
        return params;
    }
}
