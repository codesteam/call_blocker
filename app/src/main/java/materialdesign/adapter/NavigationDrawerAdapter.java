package materialdesign.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.holo.callblocker.R;

import java.util.Collections;
import java.util.List;

import materialdesign.model.NavDrawerItem;


/**
 * Created by Ravi Tamada on 12-03-2015.
 */
public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder> {
    List<NavDrawerItem> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    private int rowSelectorId;

    public NavigationDrawerAdapter(Context context, List<NavDrawerItem> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void setRowSelectorId(int rowSelectorId){
        this.rowSelectorId = rowSelectorId;
    }
    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.nav_drawer_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NavDrawerItem current = data.get(position);
        holder.rowHolder.setBackgroundResource(rowSelectorId);
        holder.title.setText(current.getTitle());

        if (current.getSubTitle() != null){
            holder.subTitle.setVisibility(View.VISIBLE);
            holder.subTitle.setText(current.getSubTitle());
        }else{
            holder.subTitle.setVisibility(View.GONE);
        }

        holder.ivItem.setBackgroundResource(current.getSelectorImageView());
       /*
        holder.dividers[0].setVisibility(View.GONE);
        holder.dividers[1].setVisibility(View.GONE);
        Divider[] dividers = data.get(position).getDivider();
        if (dividers != null)
        {
            for (int i = 0; i < dividers.length; i++){
                if (i < holder.dividers.length){
                    if (dividers[i].getGravity() <= holder.dividers.length){
                        holder.dividers[dividers[i].getGravity()].setVisibility(View.VISIBLE);
                        LinearLayout.LayoutParams lp;
                        if (dividers[i].getText() == null){
                            View v = new View(context);
                            lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                    dividers[i].getHeight());
                            v.setLayoutParams(lp);
                            v.setBackgroundColor(dividers[i].getDividerColor());;
                            holder.dividers[dividers[i].getGravity()].addView(v);
                        }
                        else {
                            lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);
                            TextView tv = new TextView(context);
                            tv.setLayoutParams(lp);
                            tv.setText(dividers[i].getText());
                            tv.setTextColor(dividers[i].getTextColor());
                            tv.setTextSize(dividers[i].getTextSize());
                            tv.setGravity(Gravity.CENTER);
                            tv.setBackgroundColor(dividers[i].getDividerColor());;
                            holder.dividers[dividers[i].getGravity()].addView(tv);

                        }
                    }
                }
            }
        }*/
        //holder.title.setTextSize(data.get(position).getTextSize());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView subTitle;
        ImageView ivItem;
        LinearLayout[] dividers = new LinearLayout[2];
        LinearLayout rowHolder;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            subTitle = (TextView) itemView.findViewById(R.id.subtTitle);

            ivItem = (ImageView) itemView.findViewById(R.id.iv_item);
            dividers[0] = (LinearLayout) itemView.findViewById(R.id.divider_top);
            dividers[1] = (LinearLayout) itemView.findViewById(R.id.divider_bellow);
            rowHolder = (LinearLayout) itemView.findViewById(R.id.ll_row_holder);
        }
    }
}
