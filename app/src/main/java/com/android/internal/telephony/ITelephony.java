package com.android.internal.telephony;

/**
 * Created by water on 4/17/16.
 */
public interface ITelephony {
    boolean endCall();

    void answerRingingCall();

    void silenceRinger();
}
